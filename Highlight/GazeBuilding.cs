using System;
using UnityEngine;


#if AUG_USE_META
using Meta;
/// <summary>
/// An example Monobehaviour which uses the gaze events to change the color of the game object it is assigned to.
/// </summary>
public class GazeBuilding : MetaBehaviour, IGazeStartEvent, IGazeEndEvent
{
#elif AUG_USE_HOLOLENS
using HoloToolkit.Unity.InputModule;

public class GazeBuilding : MonoBehaviour, IPointerSpecificFocusable
{
#endif
    /// <summary>
    /// The amout of time we have been gazing at this item.
    /// </summary>
    private float gazeTime = 0.0f;

    private bool finalisedAlpha = false;

    /// <summary>
    /// The information to display when this item is gazed at.
    /// </summary>
    public GameObject informationDisplayPrefab;

    /// <summary>
    /// Instance of prefab.
    /// </summary>
    private GameObject _displayedInfo;

    /// <summary>
    /// Information to display when gazing at this item.
    /// </summary>
	public string informationText;

    /// <summary>
    /// Further information to display when gazing at this item.
    /// </summary>
	public string informationText2;

    /// <summary>
    /// Further information to display when gazing at this item.
    /// </summary>
	public string informationText3;

    /// <summary>
    /// The material applied to the object for rendering.
    /// </summary>
    private Material _renderMaterial;

    /// <summary>
    /// The color of the object when it is intended to appear dull.
    /// </summary>
    private Color _colorDull = new Color(0.05f, 0.05f, 0.05f);

    /// <summary>
    /// The color of the object when it is intended to appear highlighted.
    /// </summary>
    private Color _colorHighlighted;

    /// <summary>
    /// The interpolation variable for defining a color between dull and highlighted.
    /// </summary>
    private float _lambda = 0f;

    /// <summary>
    /// The amount by which to change _lambda every update frame.
    /// </summary>
    private float _delta = 0.05f;

    /// <summary>
    /// Whether the user is gazing on this object.
    /// </summary>
    private bool _bIsGazing;

    private void Start()
    {
        //Obtain a reference to the material assigned to this game object. 
        Renderer renderer = GetComponent<Renderer>();
        _renderMaterial = renderer.material;
        _colorDull = _renderMaterial.color;

        //Calculate a random highlighted color for this game object
        _colorHighlighted = Color.HSVToRGB(UnityEngine.Random.Range(0f, 1f), 1f, 1f);
    }

    private void Update()
    {
        ChangeColor();

        gazeTime += Time.deltaTime;

        if (_bIsGazing)
        {
            if (_displayedInfo == null && informationDisplayPrefab != null)
            {
                _displayedInfo = Instantiate(informationDisplayPrefab, this.transform);
                _displayedInfo.GetComponentInChildren<UnityEngine.UI.Text>().text = informationText;
                _displayedInfo.GetComponentInChildren<UnityEngine.UI.Text>().text += "\n" + informationText2;
                _displayedInfo.GetComponentInChildren<UnityEngine.UI.Text>().text += "\n" + informationText3;
            }

            if (gazeTime < 1.0f)
            {
                ChangeAlphaInChildren(Time.deltaTime);
            }
            else if (!finalisedAlpha)
            {
                ChangeAlphaInChildren(1.0f);
                finalisedAlpha = true;
            }

            _displayedInfo.transform.LookAt(Camera.main.transform);
        }
        if (!_bIsGazing)
        {
            if (_displayedInfo)
            {
                if (!finalisedAlpha)
                    ChangeAlphaInChildren(-Time.deltaTime);

                if (gazeTime > 1.0f && !finalisedAlpha)
                {
                    ChangeAlphaInChildren(-1.0f);
                    Destroy(_displayedInfo);
                    _displayedInfo = null;
                    finalisedAlpha = true;
                }
            }
        }
    }

    private void ChangeAlphaInChildren(float change)
    {
        var text = _displayedInfo.GetComponentInChildren<UnityEngine.UI.Text>();
        if (text)
        {
            var c = text.color;
            text.color = new Color(c.r, c.g, c.b, Mathf.Clamp(c.a + change, 0.0f, 1.0f));
        }

        var images = _displayedInfo.GetComponentsInChildren<UnityEngine.UI.RawImage>();
        if (images != null && images.Length > 0)
        {
            foreach (var image in images)
            {
                var c = image.color;
                image.color = new Color(c.r, c.g, c.b, Mathf.Clamp(c.a + change, 0.0f, 1.0f));

                //image.material.color = _colorHighlighted;
            }
        }
    }



    /// <summary>
    /// Update the color of the game object that this Monobehaviour is attached to. 
    /// The resulting color is between the color member variables _colorDull and _colorHighlighted
    /// </summary>
    private void ChangeColor()
    {
        //A bias for the interpolation. Positive values make highlighting occur quicker and dulling occur slower.
        float lerpBias = 0.75f;

        //Whether to incrementally increase/decrease the vibrance of the object in this frame.
        float sign = _bIsGazing ? 1 : -1;

        //Modify lambda to incrementally increase/decrease the vibrance of the game object. 
        _lambda += (sign + lerpBias) * _delta;
        _lambda = Mathf.Clamp(_lambda, 0f, 2f); //Allow lambda beyond 1 so that the game object may glow for a little longer. 

        //Calculate the color of the object. Lambda greater than 1 are clamped to 1.
        Color color = Color.Lerp(_colorDull, _colorHighlighted, Mathf.Clamp(_lambda, 0f, 1f));
        _renderMaterial.color = color;
    }

#if AUG_USE_META
    /// <summary>
    /// From the IGazeStartEvent interface. This occurs when the gaze gesture begins with this object as the subject.
    /// </summary>
    void IGazeStartEvent.OnGazeStart()
    {
        GazeStart();
    }

    /// <summary>
    /// From the IGazeEndEvent interface. This occurs when the gaze gesture ends and this object is no longer the subject.
    /// </summary>
    void IGazeEndEvent.OnGazeEnd()
    {
        GazeEnd();
    }
#endif

#if AUG_USE_HOLOLENS
    void IPointerSpecificFocusable.OnFocusEnter(PointerSpecificEventData eventData)
    {
        GazeStart();
    }

    void IPointerSpecificFocusable.OnFocusExit(PointerSpecificEventData eventData)
    {
        GazeEnd();
    }
#endif

    private void GazeStart()
    {
        _bIsGazing = true;
        gazeTime = 0.0f;
        finalisedAlpha = false;
    }

    private void GazeEnd()
    {
        _bIsGazing = false;
        gazeTime = 0.0f;
        finalisedAlpha = false;
    }
}
