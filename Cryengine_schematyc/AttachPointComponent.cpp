#include "StdAfx.h"
#include "AttachPointComponent.h"
#include "StaticAutoRegistrar.h"	// for macro (unnecessary later, this is proper part of engine in 5.3)
#include <Schematyc/Entity/EntityClasses.h>	// for g_entityClassGUID (5.3)

#include <CryRenderer/IRenderer.h>
#include <CryRenderer/IRenderAuxGeom.h>
#include <CrySerialization/DynArray.h>

#include <CrySerialization/SharedPtr.h>

#include <Schematyc\Reflection\Reflection.h>
#include <Schematyc\SerializationUtils\ISerializationContext.h>
#include <CrySerialization/IArchive.h>
#include "Utils/ResourceTypes.h"

namespace AugSchematycComponents
{
	std::map <Schematyc::ObjectId, augCAttachPointComponent::SAttachPointData> augCAttachPointComponent::attachPointMap;
	std::map <EntityId, Schematyc::ObjectId> augCAttachPointComponent::idMap;

	void augCAttachPointComponent::SAttachDirection::Serialize(Serialization::IArchive& archive)
	{
		archive(direction, "direction", "Attach Direction");
		archive(bBidirectional, "bBidirectional", "Bidirectional");
	}

	void augCAttachPointComponent::SAttachPointData::Serialize(Serialization::IArchive & archive)
	{
		archive(mountingPoint, "mountPoint", "Mount Point");
		archive.doc("Point at which the attachment will occur.");

		archive(mountPointSize, "size", "Size");
		archive.doc("Size of the attachment point.");

		archive(attachPointRange, "dist", "Distance");
		archive.doc("Max distance to consider when attempting to attach.");

		archive(bThisWillAttach, "bAttach", "ThisWillAttach");
		archive.doc("True if this item will attach to another, false if the other item will attach to it");

		if (archive.openBlock("dirs", "Attachment Directions"))
		{
			archive(attachDirection, "validDir", "Valid Direction");
			archive.doc("Direction along which it is considered valid to mount this component. End of the vector corresponds with a perpendicular plane, which the other attach point must be fully beyond in order to consider the attachment.");
		
			archive.closeBlock();
		}

		archive(ignoreGeometry, "bIgnoreGeom", "Ignore Geometry");
		archive.doc("Is there any geometry we should be ignoring when attaching this entity to another?");
		if (ignoreGeometry)
		{
			archive(useDefaultGeomComponent, "bUseDefaultGeomComponent", "UseDefaultGeomComponent");
			archive.doc("Should we use a default schematyc::geom component when ignoring geometry?");

			if (!useDefaultGeomComponent)
			{
				bool invalidate = false;
				auto commonComponentSelectorContext = archive.context<AugSchematycComponents::SerializationUtils::ICommonComponentSelector>();

				if (commonComponentSelectorContext )
				{
					auto ccscId = commonComponentSelectorContext->GetId();
					if (ccscId != Schematyc::ObjectId::Invalid)
					{
						if (objId != ccscId)
						{
							if (objId != Schematyc::ObjectId::Invalid)
							{
								if (objId != (Schematyc::ObjectId)((uint32)(ccscId)-1))
								{
									invalidate = true;
								}
							}
							if (invalidate)
							{
								// When this point is reached we need to force a reupdate of the context before proceeding.
								// So set objId to Invalid and 'wait' -- force update
								//objId = Schematyc::ObjectId::Invalid;
							}
							//else
							{
								objId = ccscId;
							}
						}
					}
				}
				
				if (objId != Schematyc::ObjectId::Invalid)
				{
					auto obj = gEnv->pSchematyc->GetObject(objId);
					if (obj)
					{
						CryLog("Obj");
						if (commonComponentSelectorContext)
						{
							archive(SerializationUtils::ComponentInterfaceName(_ignoreGeomComponentGUIDString, commonComponentSelectorContext->GetPtr()), "Geom Component", "Geom Component");
						}

						ignoreGeomComponentGUIDString = _ignoreGeomComponentGUIDString;

						auto envComponent = gEnv->pSchematyc->GetEnvRegistry().GetComponent(Schematyc::GUID::FromString(_ignoreGeomComponentGUIDString));
						
#pragma region Everything in here can be deleted
						
#pragma endregion					
					}
				}

				if (ignoreGeomComponent)
				{
					
				}
				else
				{
					archive.warning(_ignoreGeomComponentGUIDString, "Could not get component from guid. Ensure the component you have selected is valid.");
				}
			}
			
		}
	}

	augCAttachPointComponent::SProperties::SProperties()
		: attachPointData()
	{
		//commonComponentSelector = new SerializationUtils::CComponentSelector<IAttachPointIgnoreGeomComponent>();
		commonComponentSelector = std::make_shared<SerializationUtils::CComponentSelector<IAttachPointIgnoreGeomComponent>>();
	}

	augCAttachPointComponent::SProperties::~SProperties()
	{
		//delete commonComponentSelector;
		//commonComponentSelector.reset();
	}

	void augCAttachPointComponent::SProperties::Serialize(Serialization::IArchive& archive)
	{
		//archive(componentSelector);
		//Serialization::SContext context(archive, &componentSelector);
		Serialization::SContext contextCommon(archive, commonComponentSelector.get());
		archive(attachPointData, "AttachPointData", "Attach Point Data");
	}

	void augCAttachPointComponent::SPreviewProperties::Serialize(Serialization::IArchive& archive)
	{
		archive(bShowMountPoints, "bShowMountPoints", "Show Mount Points");
		archive(bShowAttachDirections, "bShowAttachDirections", "Show Attach Directions");
	}

	void augCAttachPointComponent::CPreviewer::SerializeProperties(Serialization::IArchive& archive)
	{
		archive(m_properties, "properties", "Attach Point Component");
	}

	void augCAttachPointComponent::CPreviewer::Render(const Schematyc::IObject& object, const Schematyc::CComponent& component, const SRendParams& params, const SRenderingPassInfo& passInfo) const
	{
		const augCAttachPointComponent& attachPointComponent = static_cast<const augCAttachPointComponent&>(component);
		if (m_properties.bShowMountPoints)
		{
			attachPointComponent.RenderPoint();
		}
		if (m_properties.bShowAttachDirections)
		{
			const augCAttachPointComponent::SProperties* attachPointProperties = static_cast<const augCAttachPointComponent::SProperties*>(attachPointComponent.GetProperties());
			
			attachPointComponent.RenderAttachDirection(attachPointProperties->attachPointData.attachDirection);
		}
	}


	void augCAttachPointComponent::Register(Schematyc::IEnvRegistrar& registrar)
	{
		Schematyc::CEnvRegistrationScope scope = registrar.Scope(Schematyc::g_entityClassGUID);
		{
			//auto pThingy = SCHEMATYC

			auto pComponent = SCHEMATYC_MAKE_ENV_COMPONENT(augCAttachPointComponent, "AttachPoint");
			pComponent->SetDescription("Component for an attachment point. Either end.");
			pComponent->SetIcon("icons:schematyc/entity_physics_component.ico");
			pComponent->SetAuthor("Augmea Inc.");
			pComponent->SetFlags({ /*Schematyc::EEnvComponentFlags::Singleton, Schematyc::EEnvComponentFlags::Transform*/ Schematyc::EEnvComponentFlags::None });
			pComponent->SetProperties(SProperties());
			pComponent->SetPreviewer(CPreviewer());
			scope.Register(pComponent);

			Schematyc::CEnvRegistrationScope componentScope = registrar.Scope(pComponent->GetGUID());
			//Functions
			{
				auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::GetMountPoint, "648C2D83-861F-441F-BFE9-7BCBE0059E57"_schematyc_guid, "GetMountPoint");
				pFunction->SetDescription("Get the point at which the component will be mounted.");
				pFunction->BindOutput(0, 'vMnt', "MountPoint", "The point at which the entity will be mounted");
				pFunction->BindInput(1, 'apd', "AttachPointData", "AttachPointData");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::GetAttachDirection, "372D63DC-0EDE-4EDA-A371-D09C06AB238D"_schematyc_guid, "GetAttachDirection");
				pFunction->SetDescription("Get the direction from the mount point to the last bit of problematic geometry in the direction along which we will try to attach.");
				pFunction->BindOutput(0, 'vAtt', "AttachDirection", "The direction along which to try to attach.");
				pFunction->BindInput(1, 'apd', "AttachPointData", "AttachPointData");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::GetAttachLocation, "E7B1AC2F-9232-4A0F-AC46-9A84E3207395"_schematyc_guid, "GetAttachLocation");
				pFunction->SetDescription("Get the position of the attach location.");
				pFunction->BindOutput(0, 'vAtt', "AttachLocation", "The location at which to try to attach.");
				pFunction->BindInput(1, 'apd', "AttachPointData", "AttachPointData");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::WillAttachTo, "D6EC4F04-814B-4F4C-9432-06AB3CF1DD51"_schematyc_guid, "WillAttachTo");
				pFunction->SetDescription("Will this object attach to another one?");
				pFunction->BindOutput(0, 'bAt2', "WillAttachTo", "Will this object attach to another one?");
				pFunction->BindInput(1, 'apd', "AttachPointData", "AttachPointData");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::WillReceiveAttachment, "69CDE667-B35D-4826-953C-5889EB11AEC8"_schematyc_guid, "WillReceiveAttachment");
				pFunction->SetDescription("Will this object have another object attached to it?");
				pFunction->BindOutput(0, 'bAtF', "GetAttachFrom", "Will this object have another object attached to it?");
				pFunction->BindInput(1, 'apd', "AttachPointData", "AttachPointData");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::IsInRangeOfOther, "6A866085-140E-4610-A1C1-A7E2A796206E"_schematyc_guid, "IsInRangeOfOther");
				pFunction->SetDescription("Is this object in range of another attach point?");
				pFunction->BindOutput(0, 'bRng', "InRange", "Is this object in range of another attach point?");
				pFunction->BindInput(1, 'othr', "EntityId", "EntityId of entity with attach point component to check against");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::IsInValidDirectionOfOther, "A8693D8B-0933-4244-9959-2FBBFFEBB518"_schematyc_guid, "IsInValidDirectionOfOther");
				pFunction->SetDescription("Is this object on the correct side of another attach point?");
				pFunction->BindOutput(0, 'bVld', "ValidDirection", "Is this object on the correct side of another attach point?");
				pFunction->BindInput(1, 'othr', "EntityId", "EntityId of entity with attach point component to check against");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::Attach, "6B8EB109-6EBD-404E-904E-F9B813728F46"_schematyc_guid, "Attach");
				pFunction->SetDescription("Try to attach the two objects.");
				pFunction->BindOutput(0, 'bool', "Attached", "Is this object now attached?");
				pFunction->BindInput(1, 'othr', "EntityId", "EntityId of entity with attach point component to attach");
				pFunction->BindInput(2, 'atch', "ShouldAttach", "Attach (true), Detach (false)");
				componentScope.Register(pFunction);

				pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCAttachPointComponent::SendAttachPointDataSignal, "D676366C-8414-4E1D-B487-7478981A91F2"_schematyc_guid, "SendAttachPointDataSignal");
				pFunction->SetDescription("Send attach point data signal.");
				pFunction->BindInput(1, 'eid', "EntityId", "EntityId of other object with attachpoints");
				pFunction->SetName("Send AttachPoint Data Signal Name");
				componentScope.Register(pFunction);
			}
			//Signal
			{
				auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL_TYPE(augCAttachPointComponent::AttachPointDataSignal, "AttachPointSignal");
				pSignal->SetDescription("Send attach point data");
				pSignal->SetAuthor("Augmea Inc.");
				pSignal->SetName("AttachPointSignal");
				
				componentScope.Register(pSignal);
			}
		}
	}

	bool augCAttachPointComponent::Init()
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		//if (pProperties->validAttachDirections)
		if (pProperties)
		{
			// there is an ordering problem here -- I need to Init the objectId and commonComponentSelctorId
			// but at this point, Serialization is happening before executing this part
			// need to find a better way to ensure correctness.
			SAttachPointData apd = pProperties->attachPointData;
			apd.objId = Schematyc::CComponent::GetObject().GetId();
			pProperties->commonComponentSelector->SetId(apd.objId);
			auto validator = gEnv->pSchematyc->CreateValidatorArchive(Schematyc::SValidatorArchiveParams());
			apd.Serialize(*validator);

			auto& entity = Schematyc::EntityUtils::GetEntity(*this);
			attachPointMap.insert(std::make_pair(apd.objId, apd));
		}

		return true;
	}

	Vec3 augCAttachPointComponent::GetMountPoint(SAttachPointData apd) const
	{
		return apd.mountingPoint;
	}

	Vec3 augCAttachPointComponent::GetAttachDirection(SAttachPointData apd) const
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		return apd.attachDirection.direction;
	}

	Vec3 augCAttachPointComponent::GetAttachLocation(SAttachPointData apd) const
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		return apd.mountingPoint + apd.attachDirection.direction;
	}

	bool augCAttachPointComponent::WillAttachTo(SAttachPointData apd) const
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		return apd.bThisWillAttach;
	}

	bool augCAttachPointComponent::WillReceiveAttachment(SAttachPointData apd) const
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		return !apd.bThisWillAttach;
	}

	bool augCAttachPointComponent::IsInRangeOfOther(Schematyc::ExplicitEntityId other) const
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		const SAttachPointData thisApd = SAttachPointData(pProperties->attachPointData);
		const SAttachPointData otherApd = GetAttachPointData(other);

		//GetTransforms
		IEntity* pOtherEntity = gEnv->pEntitySystem->GetEntity((EntityId)other);
		auto otherTM = pOtherEntity->GetWorldTM();

		IEntity* pThisEntity =  &Schematyc::EntityUtils::GetEntity(*this);
		auto thisTM = pThisEntity->GetWorldTM();

		//Work out relative distance based on location of attach points
		auto otherAttachPoint = otherTM.TransformPoint(GetAttachLocation(otherApd));
		auto thisAttachPoint = thisTM.TransformPoint(GetAttachLocation(thisApd));

		float dist = otherAttachPoint.GetDistance(thisAttachPoint);

		//see if those ranges are within the the thresholds for each set of attachPointData
		if (dist < thisApd.attachPointRange && dist < otherApd.attachPointRange)
			return true;

		return false;
	}

	bool augCAttachPointComponent::IsInValidDirectionOfOther(Schematyc::ExplicitEntityId other) const
	{
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		const SAttachPointData thisApd = SAttachPointData(pProperties->attachPointData);
		const SAttachPointData otherApd = GetAttachPointData(other);

		//GetTransforms
		IEntity* pOtherEntity = gEnv->pEntitySystem->GetEntity((EntityId)other);
		auto otherTM = pOtherEntity->GetWorldTM();

		IEntity* pThisEntity =  &Schematyc::EntityUtils::GetEntity(*this);
		auto thisTM = pThisEntity->GetWorldTM();

		//Get AttachLocations
		auto otherAttachPoint = otherTM.TransformPoint(GetAttachLocation(otherApd));
		auto  thisAttachPoint =  thisTM.TransformPoint(GetAttachLocation(thisApd));

		//GetMountPoints
		auto otherMountPoint = otherTM.TransformPoint(GetMountPoint(otherApd));
		auto  thisMountPoint =  thisTM.TransformPoint(GetMountPoint(thisApd));

		//GetAttachDirections
		auto otherAttachDirection = otherTM.TransformVector(GetAttachDirection(otherApd));
		auto  thisAttachDirection =  thisTM.TransformVector(GetAttachDirection( thisApd));


		auto OA2TM = otherAttachPoint -  thisMountPoint;
		auto TA2OM =  thisAttachPoint - otherMountPoint;

		float dotA =  thisAttachDirection.dot(OA2TM);
		float dotB = otherAttachDirection.dot(TA2OM);

		// make check
		if (dotA > 0 && dotB > 0)
			return true;

		return false;
	}

	bool augCAttachPointComponent::Attach(Schematyc::ExplicitEntityId other, const bool attach)
	{
		if (attach)
		{
			if (IsInRangeOfOther(other) && IsInValidDirectionOfOther(other))
			{
				const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
				const SAttachPointData thisApd = SAttachPointData(pProperties->attachPointData);
				const SAttachPointData otherApd = GetAttachPointData(other);
				
				IEntity* pOtherEntity = gEnv->pEntitySystem->GetEntity((EntityId)other);
				IEntity* pThisEntity = &Schematyc::EntityUtils::GetEntity(*this);

				IEntity* pAttachingEntity	= thisApd.bThisWillAttach ? pThisEntity : otherApd.bThisWillAttach ? pOtherEntity : nullptr;
				IEntity* pAttachedToEntity	= !thisApd.bThisWillAttach ? pThisEntity : !otherApd.bThisWillAttach ? pOtherEntity : nullptr;
				
				if (!pAttachingEntity || !pAttachedToEntity)
					return false;

				const SAttachPointData attachingApd = thisApd.bThisWillAttach ? thisApd : otherApd;
				const SAttachPointData attachedToApd = !thisApd.bThisWillAttach ? thisApd : otherApd;
				
				// work out transform necessary to put the mount point belonging to this entity, at the same position as the mount point belonging to the other entity
				// Not just a move - have to rotate so that the two attach directions are (when normalised) in opposition to one another.
				// we only want to move the entity we have marked as "Attaching"

				//GetTransforms				
				auto attachingTM = pAttachingEntity->GetWorldTM();
				auto attachedToTM = pAttachedToEntity->GetWorldTM();
				
				//GetMountPoints
				auto attachingMountPoint = attachingTM.TransformPoint(GetMountPoint(attachingApd));
				auto  attachedToMountPoint =  attachedToTM.TransformPoint(GetMountPoint(attachedToApd));

				//GetAttachDirections
				auto attachingAttachDirection = attachingTM.TransformVector(GetAttachDirection(attachingApd)).GetNormalized();
				auto  attachedToAttachDirection =  attachedToTM.TransformVector(GetAttachDirection( attachedToApd)).GetNormalized();

				//Translate
				auto translateMountPoints = attachedToMountPoint - attachingMountPoint;
				Quat q = Quat::CreateRotationV0V1(-attachingAttachDirection, attachedToAttachDirection);

				Matrix34 m34 = Matrix34(q);
				Matrix34 finalMM = m34 * attachingTM;

				auto newAttachingMountPointPos = finalMM.TransformPoint(GetMountPoint(attachingApd));
				translateMountPoints = attachedToMountPoint - newAttachingMountPointPos;
				finalMM.AddTranslation(translateMountPoints);

				pAttachingEntity->SetWorldTM(finalMM);

				auto pPhys = pAttachingEntity->GetPhysicalEntity();

				pe_params_flags pf;
				pf.flagsOR = pef_ignore_areas;
				pPhys->SetParams(&pf);

				pe_simulation_params pp;
				pPhys->GetParams(&pp);
				pp.gravity = Vec3(ZERO);
				pp.gravityFreefall = Vec3(ZERO);
				pPhys->SetParams(&pp);


				// mark entity as attached
				m_pAttachedEntity = gEnv->pEntitySystem->GetEntity((EntityId)other);
				return true;
			}
		}
		else if (m_pAttachedEntity)
		{
			const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
			const SAttachPointData thisApd = SAttachPointData(pProperties->attachPointData);
			const SAttachPointData otherApd = GetAttachPointData((Schematyc::ExplicitEntityId)m_pAttachedEntity->GetId());
				
			IEntity* pOtherEntity = m_pAttachedEntity;
			IEntity* pThisEntity = &Schematyc::EntityUtils::GetEntity(*this);

			IEntity* pDetachingEntity	= thisApd.bThisWillAttach ? pThisEntity : otherApd.bThisWillAttach ? pOtherEntity : nullptr;
			IEntity* pDetachFromEntity	= !thisApd.bThisWillAttach ? pThisEntity : !otherApd.bThisWillAttach ? pOtherEntity : nullptr;
				
			if (!pDetachingEntity || !pDetachFromEntity)
				return false;

			const SAttachPointData detachingApd = thisApd.bThisWillAttach ? thisApd : otherApd;
			const SAttachPointData detachFromApd = !thisApd.bThisWillAttach ? thisApd : otherApd;
				
			// work out transform necessary to put the attach points touching
			// much easier, as if the entities are attched the rotations are correct already, and we already have the necessary vectors stored in AttachPointData

			//GetTransforms				
			auto detachingTM = pDetachingEntity->GetWorldTM();
			auto detachFromTM = pDetachFromEntity->GetWorldTM();
						
			//GetAttachDirections
			auto detachingDetachDirection = detachingTM.TransformVector(GetAttachDirection(detachingApd));
			auto  detachFromDetachDirection =  detachFromTM.TransformVector(GetAttachDirection(detachFromApd));

			auto finalTM = detachingTM.AddTranslation(-detachingDetachDirection);
			finalTM = finalTM.AddTranslation(detachFromDetachDirection);

			pDetachingEntity->SetWorldTM(finalTM);


			auto pPhys = pDetachingEntity->GetPhysicalEntity();
			pe_params_flags pf;
			pf.flagsOR = pef_ignore_areas;
			pPhys->SetParams(&pf);

			pe_simulation_params pp;
			pPhys->GetParams(&pp);
			pp.gravity = Vec3(0.f, 0.f, -9.81f);
			pp.gravityFreefall = Vec3(0.f, 0.f, -9.81f);
			pPhys->SetParams(&pp);

			// mark entity as unattached
			m_pAttachedEntity = nullptr;
		}

		return false;
	}
	
	//static
	const augCAttachPointComponent::SAttachPointData augCAttachPointComponent::GetAttachPointData(Schematyc::ExplicitEntityId other)
	{
		Schematyc::ObjectId id;
		auto _idItr = idMap.find((EntityId)other);
		if (_idItr != idMap.end())
		{
			id = _idItr->second;
		}
		else
			return SAttachPointData();

		auto apd = attachPointMap.find(id);
		if (apd != attachPointMap.end())
		{
			return apd->second;
		}

		return SAttachPointData();
	}

	void augCAttachPointComponent::SendAttachPointDataSignal(Schematyc::ExplicitEntityId id) const
	{
		auto attachPointData = GetAttachPointData(id);
		if (attachPointData.objId != Schematyc::ObjectId::Invalid)
		{
			// this sends the signal to the current entity
			AttachPointDataSignal signal = AttachPointDataSignal(attachPointData, id);
			Schematyc::CComponent::GetObject().ProcessSignal<AttachPointDataSignal>(signal);

			// This should send the signal to another (specific) entity
			/*Schematyc::StackRuntimeParams params;
			const Schematyc::CCommonTypeInfo& typeInfo = Schematyc::GetTypeInfo<AttachPointDataSignal>();
			if (typeInfo.GetClassification() == Schematyc::ETypeClassification::Class)
			{
				for (const Schematyc::SClassTypeInfoMember& member : static_cast<const Schematyc::CClassTypeInfo&>(typeInfo).GetMembers())
				{
					params.SetInput(member.id, Schematyc::CAnyConstRef(member.typeInfo, reinterpret_cast<const uint8*>(&signal) + member.offset));
				}
			}
			gEnv->pSchematyc->SendSignal(attachPointData.objId, typeInfo.GetGUID(), params);*/
		}
	}

	void augCAttachPointComponent::RenderPoint() const
	{
		IRenderAuxGeom& renderAuxGeom = *gEnv->pRenderer->GetIRenderAuxGeom();
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		
		renderAuxGeom.DrawSphere(pProperties->attachPointData.mountingPoint, pProperties->attachPointData.mountPointSize, ColorB(0, 150, 0, 128));
	}

	void augCAttachPointComponent::RenderAttachDirection(const SAttachDirection& dir) const
	{
		IRenderAuxGeom& renderAuxGeom = *gEnv->pRenderer->GetIRenderAuxGeom();
		const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		
		ColorB greenSolid(0, 150, 0, 128);
		renderAuxGeom.DrawLine(pProperties->attachPointData.mountingPoint, greenSolid, pProperties->attachPointData.mountingPoint + dir.direction, greenSolid, 1.f);

		ColorB redTranslucent(150, 0, 0, 64);
		ColorB greenTranslucent(0, 150, 0, 64);

		//Draw plane at end
		//Slightly inefficient, but easy to conceptualise
		Vec3 endPoint = pProperties->attachPointData.mountingPoint + dir.direction;		
		Matrix33 orthog = Matrix33::CreateOrthogonalBase(dir.direction.GetNormalized());
		renderAuxGeom.SetRenderFlags(e_Def3DPublicRenderflags | e_AlphaBlended);
		// Front face
		renderAuxGeom.DrawTriangle(endPoint + orthog.GetColumn1(), redTranslucent, endPoint + orthog.GetColumn2(), redTranslucent, endPoint, redTranslucent);
		renderAuxGeom.DrawTriangle(endPoint + orthog.GetColumn2(), redTranslucent, endPoint - orthog.GetColumn1(), redTranslucent, endPoint, redTranslucent);
		renderAuxGeom.DrawTriangle(endPoint - orthog.GetColumn1(), redTranslucent, endPoint - orthog.GetColumn2(), redTranslucent, endPoint, redTranslucent);
		renderAuxGeom.DrawTriangle(endPoint - orthog.GetColumn2(), redTranslucent, endPoint + orthog.GetColumn1(), redTranslucent, endPoint, redTranslucent);
		//Back face
		renderAuxGeom.DrawTriangle(endPoint + orthog.GetColumn2(), greenTranslucent, endPoint + orthog.GetColumn1(), greenTranslucent, endPoint, greenTranslucent);
		renderAuxGeom.DrawTriangle(endPoint - orthog.GetColumn1(), greenTranslucent, endPoint + orthog.GetColumn2(), greenTranslucent, endPoint, greenTranslucent);
		renderAuxGeom.DrawTriangle(endPoint - orthog.GetColumn2(), greenTranslucent, endPoint - orthog.GetColumn1(), greenTranslucent, endPoint, greenTranslucent);
		renderAuxGeom.DrawTriangle(endPoint + orthog.GetColumn1(), greenTranslucent, endPoint - orthog.GetColumn2(), greenTranslucent, endPoint, greenTranslucent);
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&augCAttachPointComponent::Register);	
}