#include "StdAfx.h"
#include "Plugin.h"

// Included only once per DLL module.
#include <CryCore/Platform/platform_impl.inl>

// 5.4 - replace the includes below
//#include <CrySchematyc/Env/IEnvRegistry.h>
//#include <CrySchematyc/Env/EnvPackage.h>
//#include <CrySchematyc/Utils/SharedString.h>

// in 5.3 the CrySchematyc interface is not yet exposed through CryCommon
#include "../../CryEngine/CrySchematyc/Core/Interface/Schematyc/Env/IEnvRegistry.h"
#include "../../CryEngine/CrySchematyc/Core/Interface/Schematyc/Env/EnvPackage.h"
#include "../../CryEngine/CrySchematyc/Core/Interface/Schematyc/Utils/SharedString.h"

// this is exposed elsewhere in 5.4, but necesary for now whil Schemtyc is still in Beta
#include "../../CryEngine/CrySchematyc/Core/Interface/CrySchematyc/CoreAPI.h"
// this is a temp replacement for something we don't need to do in 5.4
#include "StaticAutoRegistrar.h"	

#include "CrySystem\ICryPlugin.h"
#include <CryExtension\ICryPluginManager.h>
#include "ICrySensorSystemPlugin.h"
#include <ISensorTagLibrary.h>
// these global EntityRegistrator ptrs also disappear when 5.4 update arrives
IEntityRegistrator *IEntityRegistrator::g_pFirst = nullptr;
IEntityRegistrator *IEntityRegistrator::g_pLast = nullptr;

CPlugin::~CPlugin()
{
	gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);

	// 5.4 - adds the Unregister code below
	// In 5.3, the deregisterPackage fn has not yet been implemented
	/*if (gEnv->pSchematyc)
	{
		gEnv->pSchematyc->GetEnvRegistry().DeregisterPackage(GetSchematycPackageGUID());
	}*/

	IEntityRegistrator* pTemp = IEntityRegistrator::g_pFirst;
	while (pTemp != nullptr)
	{
		pTemp->Unregister();
		pTemp = pTemp->m_pNext;
	}
}

bool CPlugin::Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams)
{
	gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this);

	return true;
}

void CPlugin::OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
{
	switch (event)
	{
	case ESYSTEM_EVENT_REGISTER_SCHEMATYC_ENV:
	{
		// Register all components that belong to this plug-in
		//auto staticAutoRegisterLambda = [](Schematyc::IEnvRegistrar& registrar)	// 5.4 no need to wrap this in the FromLambda call aparently, although if it's there, it shouldn't hurt. Makes determining the type easier
		auto staticAutoRegisterLambda = Schematyc::EnvPackageCallback::FromLambda([](Schematyc::IEnvRegistrar& registrar)
		{
			// Call all static callback registered with the CRY_STATIC_AUTO_REGISTER_FUNCTION	
			// CStaticAutoRegistrar is a CStaticInstanceList<CStaticAutoRegistrar< T >>		--	newly defined in 5.4 (CryCommon/CryCore/StaticInstanceList.h) (won't need the StaticAutoRegistrar.h included above)
			Detail::CStaticAutoRegistrar<Schematyc::IEnvRegistrar&>::InvokeStaticCallbacks(registrar);		
		});

		if (gEnv->pSchematyc)
		{
			// 5.4
			gEnv->pSchematyc->GetEnvRegistry().RegisterPackage(

				stl::make_unique<Schematyc::CEnvPackage>(
					GetSchematycPackageGUID(),
					"AugmeaEntityComponents",
					//"Augmea Inc.",								// author		-- in 5.4
					//"Augmea Entity Components Package",			// description	-- in 5.4
					staticAutoRegisterLambda
					)
			);
			// the following macro can also be called, to do exactly the same as the above line.
			//gEnv->pSchematyc->GetEnvRegistry().RegisterPackage(SCHEMATYC_MAKE_ENV_PACKAGE(GetSchematycPackageGUID(), "AugmeaEntityComponents", staticAutoRegisterLambda));			
		}

		// proof of concept
		auto pSensorSystem = gEnv->pSystem->GetIPluginManager()->QueryPlugin<ICrySensorSystemPlugin>();
		if (pSensorSystem)
		{
			pSensorSystem->GetSensorSystem().GetTagLibrary().CreateTag("AugTag");
		}

	}
	break;

	case ESYSTEM_EVENT_GAME_POST_INIT:
	{
		// Register entities
		IEntityRegistrator* pTemp = IEntityRegistrator::g_pFirst;
		while (pTemp != nullptr)
		{
			pTemp->Register();
			pTemp = pTemp->m_pNext;
		}
	}
	break;
	}
}

CRYREGISTER_SINGLETON_CLASS(CPlugin)