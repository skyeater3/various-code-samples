#pragma once

#include "StdAfx.h"
#include "MeshExplodeComponent.h"
#include "StaticAutoRegistrar.h"	// for macro (unnecessary later, this is proper part of engine in 5.3)
#include "Schematyc\Entity\EntityClasses.h"
#include <CrySchematyc\STDEnvAPI.h>

#include "Cry3DEngine/IIndexedMesh.h"
#include "Cry3DEngine/IStatObj.h"

#include <CryRenderer/IRenderer.h>
#include <CryRenderer/IRenderAuxGeom.h>
#include <CryInput\IInput.h>
#include <CryInput\IHardwareMouse.h>

#include "IAttachPointIgnoreGeomComponent.h"

namespace AugSchematycComponents
{
	augCMeshExplodeComponent::SProperties::SProperties()
	{		
	}

	void augCMeshExplodeComponent::SProperties::Serialize(Serialization::IArchive& archive)
	{
		archive(Schematyc::SerializationUtils::GeomPath(m_fileName), "Geometry", "Geometry");
		archive(m_run, "Run", "Run");
		archive(m_explode, "Explode", "Explode");
		archive(m_physicalize, "Physicalize", "Physicalize");
		archive(m_speed, "Speed", "Speed");
		archive(x, "X", "X");
		archive(y, "Y", "Y");
		archive(z, "Z", "Z");
	}

#include <CryRenderer\IRenderer.h>

	void augCMeshExplodeComponent::CPreviewer::Render(const Schematyc::IObject& object, const Schematyc::CComponent& component, const SRendParams& params, const SRenderingPassInfo& passInfo) const
	{
		const augCMeshExplodeComponent& attachPointComponent = static_cast<const augCMeshExplodeComponent&>(component);
		auto pProperties = static_cast<SProperties*>(component.GetProperties());

		if (attachPointComponent.OBJ)
		{
			if (pProperties->m_explode)
			{
				//attachPointComponent.ExOBJ->Render(params, passInfo);
			}
			else
			{
				attachPointComponent.OBJ->Render(params, passInfo);
			}
		}
	}

	bool augCMeshExplodeComponent::Init()
	{
		//auto pPreviewer = static_cast<CPreviewer*>(GetPreviewer());

		m_explodeOrigin = Vec3(0.f, 0.f, 0.f);
		done = true;
		t = 0.f;
		return true;
	}

	// This is not the way to do this - Run, Init and Shutdown should be deprecated by 5.4
	// but because everything is an EntityComponent, rather than a Schematyc::CComponent, ProcessEvent's should work.
	// i.e. This will all be moved back to the sensible place it was in the non-schematyc version.
	void augCMeshExplodeComponent::Run(Schematyc::ESimulationMode mode)
	{
		switch (mode)
		{
			case Schematyc::ESimulationMode::Editor:
			{
				gEnv->pSchematyc->GetUpdateScheduler().Disconnect(m_connectionScope);
				//gEnv->pHardwareMouse->RemoveListener(static_cast<CPreviewer*>(GetPreviewer()));

				gameRunning = false;
				RemoveEntities();
				origin.clear();
				target.clear();
				entities.clear();
				break;
			}
			case Schematyc::ESimulationMode::Game:
			{
				// This stuff won't be necessary once the 5.4 update come, but for right now the only way to hook into the Update (or PrePhysicsUpdate) cycle is to register with the schematyc system
				//gEnv->pSchematyc->GetUpdateScheduler().Connect(Schematyc::SUpdateParams(Schematyc::Delegate::Make(*this, &augCMeshExplodeComponent::Update), m_connectionScope));
				gEnv->pSchematyc->GetUpdateScheduler().Connect(Schematyc::SUpdateParams(Schematyc::Delegate::Make(*this, &augCMeshExplodeComponent::PrephysicsUpdate), m_connectionScope, (Schematyc::UpdateFrequency) 0U, (Schematyc::UpdatePriority) (Schematyc::EUpdateStage::PrePhysics | Schematyc::EUpdateDistribution::Default)));

				gameRunning = true;
				Reset(2);		
				
				break;
			}
			case Schematyc::ESimulationMode::Preview:
			{
				//gEnv->pSchematyc->GetUpdateScheduler().Connect(Schematyc::SUpdateParams(Schematyc::Delegate::Make(*this, &augCMeshExplodeComponent::PrephysicsUpdate), m_connectionScope, (Schematyc::UpdateFrequency) 0U, (Schematyc::UpdatePriority) (Schematyc::EUpdateStage::PrePhysics | Schematyc::EUpdateDistribution::Default)));
				Reset(0);
			}
			case Schematyc::ESimulationMode::Idle:
				break;
			
		}
	}

	void augCMeshExplodeComponent::Shutdown()
	{
		//gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);
		//gEnv->pHardwareMouse->RemoveListener(static_cast<CPreviewer*>(GetPreviewer()));
		//gEnv->pInput->SetExclusiveListener(nullptr);
		//gEnv->pSystem->UnregisterWindowMessageHandler(static_cast<CPreviewer*>(GetPreviewer()));
	}

	Schematyc::SGUID augCMeshExplodeComponent::ReflectSchematycType(Schematyc::CTypeInfo<augCMeshExplodeComponent>& typeInfo)
	{
		typeInfo.SetGUID("A25BAF72-3B50-4CB4-B408-178433417ABE"_schematyc_guid);
		typeInfo.AddBase<IAttachPointIgnoreGeomComponent>();
		return typeInfo.GetGUID();
	}

	void augCMeshExplodeComponent::RegisterMeshExplodeComponent(Schematyc::IEnvRegistrar & registrar)
	{
		Schematyc::CEnvRegistrationScope scope = registrar.Scope(Schematyc::g_entityClassGUID);
		{

			auto pComponent = SCHEMATYC_MAKE_ENV_COMPONENT(augCMeshExplodeComponent, "MeshExplode");
			pComponent->SetAuthor("Augmea Inc.");
			pComponent->SetDescription("Explodes a mesh");
			pComponent->SetIcon("shake.bmp");
			pComponent->SetFlags({ Schematyc::EEnvComponentFlags::Singleton });
			pComponent->SetProperties(SProperties());
			pComponent->SetPreviewer(CPreviewer());
			//This function call doesn't run
			//pComponent->AddDependency(Schematyc::EEnvComponentDependencyType::None, IAttachPointIgnoreGeomComponent::Schematyc_IID());
			scope.Register(pComponent);

			Schematyc::CEnvRegistrationScope componentScope = registrar.Scope(pComponent->GetGUID());		

			// The difference between this line, and the other is that registering in 'scope' caues a crash in unrelated locations	(AttachPointComponent::~SProperties)
			//																														(and ~ICommonComponentSelector)           for some unkown reason
			// IAttachPointIgnoreGeomComponent::Register(scope);	
			// That is, the SCHEMATYC_MAKE_ENV_INTERFACE macro must be called at a valid scope (although what is valid, I don't know)
			IAttachPointIgnoreGeomComponent::Register(componentScope);

			{
				auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL_TYPE(SExplodeSignal, "Explode");
				pSignal->SetAuthor("Augmea Inc.");
				pSignal->SetDescription("Sent when an entity begins to explode.");
				componentScope.Register(pSignal);

				auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCMeshExplodeComponent::SetState, "C5F1EE85-2A38-48C3-8351-C7F3FA80FCFE"_schematyc_guid, "SetState");
				pFunction->SetAuthor("Augmea Inc.");
				pFunction->SetDescription("Explodes a mesh");
				pFunction->BindInput(1, 'stat', "State", "State 0: base mesh; State 1: exploded mesh; State 2: Physics pieces");
				componentScope.Register(pFunction);				
			}
		}
	}

	void augCMeshExplodeComponent::Reset(int resetEvent)
	{
		auto pProperties = static_cast<SProperties*>(Schematyc::CComponent::GetProperties());
		m_explodeOrigin = Vec3(pProperties->x, pProperties->y, pProperties->z);

		if (pProperties->m_fileName.empty())
			return;

		if (!OBJ)
		{
			OBJ = gEnv->p3DEngine->LoadStatObj(pProperties->m_fileName);
			OBJ->AddRef();
			m_slot = 0;
		}

		if (OBJ)
		{
			switch (resetEvent)
			{
			case 0:
				if (gameRunning)
				{
					EnablePhysics(pProperties->m_physicalize);
				}
				break;
			case 2:				
				InitEntities();				
				break;
			}
		}

		state state = pProperties->m_explode ? open : close;
	
		if (pProperties->m_physicalize)
		{
			targetState = state;
			currentState = targetState;
			t = 0.0f;
		}


		if (!pProperties->m_physicalize /*&& targetState != state*/)
		{
			targetState = state;
			currentState = transition;
		}
		

		IEntity* pThisEntity = static_cast<IEntity*>(&Schematyc::EntityUtils::GetEntity(*this));
		
		if (!pThisEntity->IsActive())
			pThisEntity->Activate(true);
	}

	void augCMeshExplodeComponent::RecurseStatObj(IStatObj * statObj)
	{
		IMaterial* material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial("objects/radial_engine/radial_engine.mtl")->GetSubMtl(0);
		IEntity* pThisEntity = static_cast<IEntity*>(&Schematyc::EntityUtils::GetEntity(*this));
		Matrix34 entityTM = pThisEntity->GetWorldTM();
		Vec3 explodeOrigin = m_explodeOrigin + entityTM.GetTranslation();

		int surfaceIdx = material->GetSurfaceTypeId();
		int subObjectCount = statObj->GetSubObjectCount();

   		for (int i = 0; i < subObjectCount; i++)
		{
			IStatObj::SSubObject* sSubObject = statObj->GetSubObject(i);
			if (!sSubObject->pStatObj)
				continue;

			Matrix34 ltm = entityTM * sSubObject->localTM;
			origin.push_back(QuatT(ltm));

			Vec3 pos = ltm.GetTranslation();

			SEntitySpawnParams params;
			params.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
			params.vPosition = pos;
			params.qRotation = Quat(ltm);
			params.vScale = Vec3(1, 1, 1);
			params.nFlags = ENTITY_FLAG_NO_SAVE | ENTITY_FLAG_PROCEDURAL | ENTITY_FLAG_SPAWNED;
			IEntity* pEntity = gEnv->pEntitySystem->SpawnEntity(params);
			if (pEntity)
			{
				pEntity->SetName(sSubObject->name);
				pEntity->SetStatObj(sSubObject->pStatObj, 0, false);

				SubPart* pPart = new SubPart(pEntity);
				pPart->surfaceIdx = surfaceIdx;
				pPart->slot = 0;
				//TODO: for now disable physicalize on static meshes
				pPart->pPE = Physicalize(pPart, PE_STATIC);
				pPart->Physicalize();

				entities.push_back(pPart);
			}

			Vec3 dir = pos - explodeOrigin;
			//dir.Normalize();
			ltm.SetTranslation(dir + pos);
			target.push_back(QuatT(ltm));
		}
	}

	void augCMeshExplodeComponent::RemoveEntities()
	{
		if (entities.size() > 0)
		{
			for (int i = 0; i < entities.size(); i++)
			{
				delete entities[i];
			}
		}
	}

	void augCMeshExplodeComponent::InitEntities()
	{
		RemoveEntities();
		origin.clear();
		target.clear();
		entities.clear();
		RecurseStatObj(OBJ);
	}

	void augCMeshExplodeComponent::EnablePhysics(bool enable)
	{
		IEntity* pThisEntity = static_cast<IEntity*>(&Schematyc::EntityUtils::GetEntity(*this));
		
		if (entities.size() > 0)
		{
			if (enable && !physicsEnabled)
			{
				for (int i = 0; i < entities.size(); i++)
				{					
					entities[i]->Unphysicalize();
					entities[i]->pPE = Physicalize(entities[i], PE_RIGID);
					entities[i]->Physicalize();
				}

				//pe_explosion explosion;
				//explosion.epicenter = explosion.epicenterImp = m_pEntity->GetWorldPos();
				//explosion.holeSize = 0;
				//explosion.impulsivePressureAtR = 500;
				//explosion.rmin = 0.5f;
				//explosion.rmax = 1.5f;
				//explosion.r = 0.5f;
				//explosion.explDir = Vec3(0, 0, 1);
				//
				//gEnv->pPhysicalWorld->SimulateExplosion(&explosion);

				physicsEnabled = true;
			}
			else if(/*!enable &&*/ physicsEnabled)
			{
				last.clear();
				if (entities.size() > 0)
				{
					for (int i = 0; i < entities.size(); i++)
					{
						entities[i]->Unphysicalize();
						//TODO: for now disable physicalize on static meshes
						entities[i]->pPE = Physicalize(entities[i], PE_STATIC);
						entities[i]->Physicalize();

						Matrix34 ltm = entities[i]->pEntity->GetWorldTM();
						last.push_back(QuatT(ltm));
					}
				}

				physicsEnabled = false;
			}
		}
	}

	IPhysicalEntity* augCMeshExplodeComponent::Physicalize(SubPart* part, pe_type type)
	{
		IStatObj* pStatObj = part->pEntity->GetStatObj(part->slot);
		AABB aabb = pStatObj->GetAABB();

		primitives::box box;
		box.Basis.SetIdentity();
		box.center = (aabb.max + aabb.min) * 0.5f;
		box.size = (aabb.max - aabb.min) * 0.5f;
		box.bOriented = 0;

		float x = abs(aabb.max.x - aabb.min.x);
		float y = abs(aabb.max.y - aabb.min.y);
		float z = abs(aabb.max.z - aabb.min.z);
		float volume = x * y * z;

		IRenderNode* pRenderNode = part->pEntity->GetRenderNode();
		IPhysicalWorld* pPhysWorld = gEnv->pPhysicalWorld;
		IGeomManager* pGeomMan = pPhysWorld->GetGeomManager();
		IPhysicalEntity* m_pPhysEnt;
	
		if (IGeometry* pBoxGeom = pGeomMan->CreatePrimitive(primitives::box::type, &box))
		{
			if (phys_geometry* pPhysGeom = pGeomMan->RegisterGeometry(pBoxGeom))
			{
				pPhysGeom->nRefCount = 0;

				if (m_pPhysEnt = pPhysWorld->CreatePhysicalEntity(type, NULL, pRenderNode, PHYS_FOREIGN_ID_ENTITY))
				{
					pe_geomparams geomParams;
					geomParams.flags = geom_collides;
					geomParams.mass = 0.0787f * volume;
					geomParams.nMats = 1;
					geomParams.pMatMapping = &(part->surfaceIdx);
					m_pPhysEnt->AddGeometry(pPhysGeom, &geomParams);

					pe_params_foreign_data foreignData;
					foreignData.pForeignData = part->pEntity;
					foreignData.iForeignData = PHYS_FOREIGN_ID_ENTITY;
					m_pPhysEnt->SetParams(&foreignData);

					pe_simulation_params simParams;
					simParams.maxLoggedCollisions = 0;
					m_pPhysEnt->SetParams(&simParams);
				}
			}
			pBoxGeom->Release();
		}

		return m_pPhysEnt;
	}

	void augCMeshExplodeComponent::SetState(int state)
	{
		auto pProperties = static_cast<SProperties*>(Schematyc::CComponent::GetProperties());
		
		switch (state)
		{
		case 0:
			pProperties->m_physicalize = false;
			pProperties->m_explode = false;
			Reset(0);
			break;
		case 1:
			pProperties->m_physicalize = false;
			pProperties->m_explode = true;
			Reset(0);
			break;
		case 2:
			pProperties->m_physicalize = true;
			Reset(0);
			break;
		}
	}

	IGeometry* augCMeshExplodeComponent::CreateMesh(IGeomManager* pGeoman, IStatObj* statObj, int MatId)
	{
		CMesh* mesh = statObj->GetRenderMesh()->GetIndexedMesh()->GetMesh();
	
		std::vector<char> faceMaterials;
		Vec3* pVertices = nullptr;
		vtx_idx* pIndices = nullptr;

		int nVerts = mesh->GetVertexCount();
		int nIndices = mesh->GetIndexCount();
		pVertices = mesh->m_pPositions;
		pIndices = mesh->m_pIndices;
	
		for (int i = 0; i < nIndices / 3; i++)
		{
			faceMaterials.push_back(MatId);
		}

		if (nIndices / 3 > 6)//2)
		{
			Vec3 ptmin = mesh->m_bbox.max;
			Vec3 ptmax = mesh->m_bbox.min;

			int flags = mesh_multicontact1;
			flags |= nIndices <= 30 ? mesh_SingleBB : mesh_OBB | mesh_AABB;
			flags |= mesh_approx_box | mesh_approx_sphere | mesh_approx_cylinder | mesh_approx_capsule;
			flags |= mesh_shared_foreign_idx;
		
			int nMinTrisPerNode = 2;
			int nMaxTrisPerNode = 4;
			Vec3 size = ptmax - ptmin;
			if (nIndices < 600 && max(max(size.x, size.y), size.z) > 6) // make more dense OBBs for large (wrt terrain grid) objects
				nMinTrisPerNode = nMaxTrisPerNode = 1;

			IGeometry* pGeom = pGeoman->CreateMesh(
				&pVertices[0],
				&pIndices[0],
				&faceMaterials[0],
				0,
				nIndices / 3,
				flags, 0.05f, nMinTrisPerNode, nMaxTrisPerNode, 2.5f);

			return pGeom;
		}
		else
			return nullptr;
	}

	void augCMeshExplodeComponent::Update(const Schematyc::SUpdateContext & updateContext)
	{
		//
	}

	void augCMeshExplodeComponent::PrephysicsUpdate(const Schematyc::SUpdateContext & updateContext)
	{
		auto pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
		IEntity* pThisEntity = static_cast<IEntity*>(&Schematyc::EntityUtils::GetEntity(*this));

		if (m_slot != -1 && pProperties->m_run && !pProperties->m_physicalize)
		{
			// Move Entities
			if (currentState == transition)
			{
				float delta = gEnv->pTimer->GetFrameTime() * pProperties->m_speed;
				bool phys = last.size() > 0;
				if (!phys)
					delta = targetState == open ? delta : -1 * delta;

				t += delta;
				t = t > 1.0f ? 1.0f : t < 0.0f ? 0.0f : t;

				int count = entities.size();
				
				for (int i = 0; i < count; i++)
				{
					QuatT trg;
					QuatT org;
					if (phys)
					{
						org = last[i];
						if (targetState == open)
						{
							trg = target[i];
						}
						else if (targetState == close)
						{
							trg = origin[i];
						}
					}
					else
					{
						org = origin[i];
						trg = target[i];
					}

					Quat rot = Quat::CreateSlerp(org.q, trg.q, t);
					Vec3 a = org.t;
					Vec3 b = trg.t;
					Matrix34 ltm;
					Vec3 pos = a * (1.0f - t) + (b * t);
					ltm.SetTranslation(pos);
					ltm.SetRotation33(Matrix33(rot));
					entities[i]->pEntity->SetWorldTM(ltm);
				}

				if (t > 0.0f && t < 1.0f)
					done = false;
				else
				{
					done = true;
					currentState = targetState;
					last.clear();
					if (phys && currentState == close)
						t = 0.0f;
				}
				//CryLogAlways("%f", t);
			}
		}
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&augCMeshExplodeComponent::RegisterMeshExplodeComponent);
}
