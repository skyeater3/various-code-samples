// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 10:31 12.06.2017 by Ali Mehmet Altundag & Luke Edwards 

#pragma once

#include <CrySystem/ICryPlugin.h>
#include <CryGame/IGameFramework.h>
#include <CryEntitySystem/IEntitySystem.h>
#include <Schematyc\Utils\GUID.h>

class CPlugin
	: public ICryPlugin
	, public ISystemEventListener
{
public:
	CRYINTERFACE_SIMPLE(ICryPlugin)
		CRYGENERATE_SINGLETONCLASS(CPlugin, "Whiteboard_", 0x37800411E19B45AC, 0x9BC6887B703E95D6)

	// the schematyc GUID's do not have to (and probably should not) match the above
	//static CryGUID GetSchematycPackageGUID() { return "{D90E16C7-9ED6-4B15-8B2A-FE12438FB5D9}"_cry_guid; }	// 5.4 _schematyc_guid is replaced with _cry_guid
	static Schematyc::SGUID GetSchematycPackageGUID() { return "D90E16C7-9ED6-4B15-8B2A-FE12438FB5D9"_schematyc_guid; }	// 5.3

		virtual ~CPlugin();

	//! Retrieve name of plugin.
	virtual const char* GetName() const override { return "Whiteboard"; }

	//! Retrieve category for the plugin.
	virtual const char* GetCategory() const override { return "Game"; }

	//! This is called to initialize the new plugin.
	virtual bool Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams) override;

	virtual void OnPluginUpdate(EPluginUpdateType updateType) override {}

	// ISystemEventListener
	virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	// ~ISystemEventListener
};


/* Notice the ComponentCreator func, it has to return
something. It has to return addr of the created component
and it s ok with one component creation but how about
multiple component creations? does caller of this
ComponentCreator care about the return val?
Apparently, not really, notice the CEntity::Init
*/


// notice the default type
template<typename T = IEntityComponent, typename...Tn>
static IEntityComponent* EntityComponentCreator(IEntity *pEntity, SEntitySpawnParams& params, void* pUserData)
{
	// recursive entity component creator. when we
	// reach the last component type creation, Tn
	// will be empty and return nullptr --doesn't
	// matter we have implemented CreateComponentClass
	// for type "IEntityComponent", we ll never call it.

	pEntity->CreateComponentClass<T>();

	// dont call last, IEntityComponent, type of entity component
	// creation func.
	if (0 == sizeof...(Tn))
		return nullptr;

	// ...next type of entity component
	EntityComponentCreator<Tn...>(pEntity, params, pUserData);
	return nullptr;
}


static IEntityClass* RegisterEntity(const char* name, IEntityClass::UserProxyCreateFunc creator, const char* editorCategory = "", const char* editorIcon = "", bool bIconOnTop = false)
{
	IEntityClassRegistry::SEntityClassDesc clsDesc;
	clsDesc.sName = name;
	clsDesc.editorClassInfo.sCategory = editorCategory;
	clsDesc.editorClassInfo.sIcon = editorIcon;
	clsDesc.editorClassInfo.bIconOnTop = bIconOnTop;
	clsDesc.pUserProxyCreateFunc = creator;
	return gEnv->pEntitySystem->GetClassRegistry()->RegisterStdClass(clsDesc);
}

struct IEntityRegistrator
{
	IEntityRegistrator()
	{
		if (g_pFirst == nullptr)
		{
			g_pFirst = this;
			g_pLast = this;
		}
		else
		{
			g_pLast->m_pNext = this;
			g_pLast = g_pLast->m_pNext;
		}
	}

	virtual void Register() = 0;
	virtual void Unregister() { }

public:
	IEntityRegistrator *m_pNext;

	static IEntityRegistrator *g_pFirst;
	static IEntityRegistrator *g_pLast;
};