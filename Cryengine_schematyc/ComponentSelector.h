#pragma once
#include "ICommonComponentSelector.h"

namespace Schematyc
{
	// forward definition for Schematyc type only used in static_assert
	class CComponent;
}

namespace AugSchematycComponents
{
	namespace SerializationUtils
	{
		template<typename TYPE>
		class CComponentSelector : public ICommonComponentSelector
		{
			using TypeConstPtr = std::shared_ptr<const TYPE>;

		private:
			static std::map<std::string, std::string> componentGuidsNames;

		public:
			CComponentSelector()
			{
				static_assert(std::is_base_of<Schematyc::CComponent, TYPE>::value, "Provided component interface must be derived from Schematyc::CComponent");
				_ptr.reset(this);
			}

			// Inherited via ICommonComponentSelector
			virtual const char * GetName() const override
			{
				const Schematyc::CTypeInfo<TYPE>& typeInfo = Schematyc::GetTypeInfo<TYPE>();
				return typeInfo.GetName().c_str();
			}
			virtual const Schematyc::SGUID GetGUID() const override
			{
				const Schematyc::CTypeInfo<TYPE>& typeInfo = Schematyc::GetTypeInfo<TYPE>();
				return typeInfo.GetGUID();
			}
		};
	}
}
