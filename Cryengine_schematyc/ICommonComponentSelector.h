#pragma once
#include <CrySerialization\Decorators\ResourceSelector.h>

#include <Schematyc\IObject.h>

#include <Schematyc\Script\IScriptElement.h>
#include <Schematyc\Script\IScriptRegistry.h>

#include <Schematyc\Runtime\IRuntimeClass.h>
#include <Schematyc\Runtime\IRuntimeRegistry.h>

#include <Schematyc\Env\Elements\IEnvComponent.h>
#include <Schematyc\Env\IEnvRegistry.h>

#include "SchematycGuidUtil.h"

using EntityId = unsigned int;

namespace AugSchematycComponents
{
	namespace SerializationUtils
	{
		struct ICommonComponentSelector;
		DECLARE_SHARED_POINTERS(ICommonComponentSelector)

		struct ICommonComponentSelector : public Serialization::ICustomResourceParams
		{
		protected:
			ICommonComponentSelector() {};

			ICommonComponentSelectorPtr _ptr;
			Schematyc::ObjectId _id;
			EntityId _eid = 0;

			// this, specifically, is mutable because adding or removing names and guids to this doesn't fundamentally alter the nature of the commonComponentSelector
			//mutable std::map<std::string, std::string> componentGuidsNames;

		public:
			virtual ~ICommonComponentSelector()
			{
			}

			virtual const char* GetName() const = 0;
			virtual const Schematyc::SGUID GetGUID() const = 0;
			
			// Get the script element which corresponds to the 'class' of the object with id stored in this ICommonComponentSelector
			const Schematyc::IScriptElement * GetObjectClassScriptElement() const
			{
				auto obj = gEnv->pSchematyc->GetObject(GetId());
				auto pRuntimeClass = &obj->GetClass();

				const Schematyc::IScriptElement * _returnVal = nullptr;

				Schematyc::ScriptElementConstVisitor visitor = Schematyc::ScriptElementConstVisitor::FromLambda([className = pRuntimeClass->GetName(), &_returnVal](const Schematyc::IScriptElement& script)
				{
					switch (script.GetElementType())
					{
						case Schematyc::EScriptElementType::Class:
							if (std::strcmp(script.GetName(), className) == 0)
							{
								_returnVal = &script;
								return Schematyc::EVisitStatus::Stop;		// will only ever be one valid class for this obj
							}
							break;
						default:
							break;
					}				
					return Schematyc::EVisitStatus::Continue;
				});

				const Schematyc::IScriptElement* element = &gEnv->pSchematyc->GetScriptRegistry().GetRootElement();
				if (element)
				{
					element->VisitChildren(visitor);
				}

				assert(_returnVal != nullptr);

				return _returnVal;
			}
			
			// Does the given component match the component type you are looking for?
			// TYPE is defined at construction of the concrete templated CComponentSelector, but we only access it via guids here
			bool ComponentHasBaseOfValidType(const Schematyc::IEnvComponent& component) const
			{
				if (GetId() != Schematyc::ObjectId::Invalid)
				{
					bool ret = false;
					auto obj = gEnv->pSchematyc->GetObject(GetId());
					auto pRuntimeClass = &obj->GetClass();
					auto pEnvClass = gEnv->pSchematyc->GetEnvRegistry().GetClass(pRuntimeClass->GetEnvClassGUID());
					
					if (obj)
					{
						// check to see if the guid of this component matches the guid we are trying to match against, early success
						auto componentGUID = component.GetGUID();
						if (componentGUID == GetGUID())
							return true;

						// otherwise we need to loop through the components children and test them for a match
						bool match = false;
						auto pChild = component.GetFirstChild();
						while (pChild)
						{
							auto elementType = pChild->GetElementType();
							if (elementType == Schematyc::EEnvElementType::Class || elementType == Schematyc::EEnvElementType::Interface)
							{
								if (pChild->GetGUID() == GetGUID())
								{
									// At this point, have found a component in the env registry that matches the component we are looking for
									// still need to see if that component is present on the object that we are checking (elsewhere)
									match = true;
									break;
								}
							}
							pChild = pChild->GetNextSibling();
						}

						return match;
					}
				}
				return false;
			}

			// Does the given component actually exist within the object with _id?
			// Checks for TYPE defined at construction of the concrete templated CComponentSelctor, but only by name
			bool ComponentIsPartOfObject(const Schematyc::IEnvComponent& component) const
			{
				// we should only get into here if the component passed as a parameter actually meets the guid requirements 
				// -- either it, or it's children (hopefully that includes subclasses and not a lot else) is a match for the component we are looking for
				// but it doesn't actually matter if you call it without checking (it's just slow)
				//
				// I guess this will throw up false positives if schematyc components have valid components in their slots (further down the **schematyc** inheritance tree, not **c++** inheritence)
				// But if that arises we'll have to find a more elegant solution (or hope 5.4 has actually been released, when I guess this will stop being an issue with everything being an **IEntityComponent**)

				auto pClassScriptElement = GetObjectClassScriptElement();
				bool ret = false;

				if (pClassScriptElement)
				{
					// this visitor recursively called
					Schematyc::ScriptElementConstVisitor visitor = Schematyc::ScriptElementConstVisitor::FromLambda(
						[componentName = component.GetName(), &ret](const Schematyc::IScriptElement& script)
					{
						switch (script.GetElementType())
						{
							// if this is a componentinstance or an interface check to see if the name matches against what we know we are looking for (from above)
							case Schematyc::EScriptElementType::ComponentInstance:
							case Schematyc::EScriptElementType::Interface:
								if (std::strcmp(script.GetName(), componentName) == 0)
								{
									ret = true;
									//return Schematyc::EVisitStatus::Continue;	// might have more that one valid component on this obj
									return Schematyc::EVisitStatus::Stop;		// but this function should just check to see if a valid component exists ???
								}
								break;
							default:
								break;
						}
						return Schematyc::EVisitStatus::Continue;
					});
				
					// loop through all childrn of the class element in the script registry and search for component matches				
					pClassScriptElement->VisitChildren(visitor);
				}
				
				return ret;
			}

			// Get the lists of names and corresponding guids that match the component you are looking for, on the object with id.
			void GetBases(std::vector<std::string> &names, std::vector<std::string> &guids) const
			{
				auto envComponentVisitorLambda = Schematyc::EnvComponentConstVisitor::FromLambda(
					[&names, &guids, this/*, guidNameLookup = &componentGuidsNames*/](const Schematyc::IEnvComponent& component)
				{
					auto guid = Schematyc::GUID::ToString(component.GetGUID());

					// is the TYPE of CComponentSelector a base class of the component we are currently looking at.
					if (ComponentHasBaseOfValidType(component) && ComponentIsPartOfObject(component))
					{
						// emplace returns a pair of <std::pair<std::string, std::string>, bool> where the first element is the inserted (or found) pair, and the bool is true or false depending on whether insertion happened.
						//auto emplaced = guidNameLookup->emplace(std::make_pair(std::string(guid), std::string(component.GetName())));

						//names.push_back(emplaced.first->second);
						//guids.push_back(emplaced.first->first);

						names.push_back(component.GetName());
						guids.push_back(guid);
					}
					else
					{
						// if this isn't a valid component and it is in the guidNameLookup, remove it
						// can be left over from other searches looking for same component type, if component is removed
						// or if we are searching for same component on a different class
						//auto itr = guidNameLookup->find(guid);
						//if (itr != guidNameLookup->end())
						//{
						//	guidNameLookup->erase(guid);
						//}
					}
					return Schematyc::EVisitStatus::Continue;
				});

				// check all components in the env registry to see if they match the component we are looking for
				gEnv->pSchematyc->GetEnvRegistry().VisitComponents(envComponentVisitorLambda);
			}

			// Get the index of the desired compnent on the object with id stored in this ICommonComponentSelector
			// That is find n, such that the nth element visited by IObject::VisitComponents is the correct CComponent
			int GetObjectComponentIndex() const { return 0; }
						

			// Get a list of script elements which correspond to all 'componentInstances' or 'interfaces' of the object with id stored in this ICommonComponentSelector, which match a given component type
			const std::list<const Schematyc::IScriptElement*> GetObjectComponentScriptElements() const
			{
				const Schematyc::IScriptElement* pClassScriptElement = GetObjectClassScriptElement();
				std::list<const Schematyc::IScriptElement*> list;


				//if (ComponentHasBaseOfValidType(component))
				//{
				//	Schematyc::ScriptElementConstVisitor visitor = Schematyc::ScriptElementConstVisitor::FromLambda([componentName = component.GetName(), &list](const Schematyc::IScriptElement& script)
				//	{
				//		switch (script.GetElementType())
				//		{
				//			// if this is a componentinstance or an interface check to see if the name matches against what we know we are looking for (from above)
				//			case Schematyc::EScriptElementType::ComponentInstance:
				//			case Schematyc::EScriptElementType::Interface:
				//				if (std::strcmp(script.GetName(), componentName) == 0)
				//				{
				//					list.push_back(script);
				//					return Schematyc::EVisitStatus::Continue;	// might have more that one valid component on this obj
				//				}
				//				break;
				//			default:
				//				return Schematyc::EVisitStatus::Continue;
				//		}
				//		return Schematyc::EVisitStatus::Continue;
				//	});
				//
				//	pClassScriptElement->VisitChildren(visitor);
				//}
				return list;
			}


			ICommonComponentSelectorPtr GetPtr() const
			{
				return _ptr;
			}

			const Schematyc::ObjectId GetId() const
			{
				return _id;
			}

			void SetId(const Schematyc::ObjectId id)
			{
				_id = id;
			}

			const EntityId GetEntityId() const
			{
				return _eid;
			}

			void SetEntityId(const EntityId id)
			{
				_eid = id;
			}

			void Serialize(Serialization::IArchive& archive)
			{
				Serialization::SContext context(archive, this);
			}
		};
	}
}