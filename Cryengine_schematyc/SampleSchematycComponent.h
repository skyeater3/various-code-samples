// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 07.07.2017 by Luke Edwards 

#pragma once
#include <CryEntitySystem\IEntityComponent.h>
#include <CryEntitySystem\IEntity.h>

#include <CrySchematyc\CoreAPI.h>

namespace AugSample
{
	class CSampleSchematycComponent
		:  public Schematyc::CComponent		// In 5.3 this is necessary (for stuff like the ComponentPreviewer among other things, and to have a schematyc object we can call upon to process signals)
		//, public IEntityComponent			// but only this is needed for 5.4 (much more useful, generally)
		// These are mutually exclusive, unfortunately. You cannot have both here, or the second will be null
	{
	public:
		CSampleSchematycComponent() = default;
		virtual ~CSampleSchematycComponent() {}

		// The following function can be defined as a static method, *or a free function defined in the same namespace
		// The name of this function must be correct (depending on version) or an error will be thrown when it cannot be found (at compile time, thankfully)
		// ( see CryEngine\CryCommon\CrySchematyc\Reflection\TypeDesc.h						(5.4)	for details	)
		//		 CryEngine\CrySchematyc\Core\Interface\Schematyc\Reflection\Reflection.h	(5.3)			
		
		//static CryGUID ReflectType(Schematyc::CTypeDesc<CSampleSchematycComponent>& desc);				//5.4 -- change in function name, return type, parameter type, but performs same task
																											// *alternatively (see SampleSignalExposedToSchematyc below)
		static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<CSampleSchematycComponent>& desc)	//5.3
		{
			desc.SetGUID(IID());

			// there is more data that can be set in the desc. But at a minimum, you must set the GUID.
			// 5.4 brings even more data that can be saved (label, description, icon etc)
			// some of which has been moved from the Register function in 5.3

			//5.4
			//desc.SetEditorCategory("Sample");
			//desc.SetLabel("Sample Schematyc Component");
			//desc.SetDescription("A sample schemtyc component for the purposes of demonstration");
			//desc.SetComponentFlags({ IEntityComponent::EFlags::Singleton });

			return IID();
		}

		// simple fn to retrieve component guid
		//static CryGUID& IID()																						//5.4
		static Schematyc::SGUID IID()																				//5.3
		{
			//static CryGUID id = "5EB31A63-F5C4-462F-9027-F9C86D98F49C"_cry_guid;									//5.4
			static Schematyc::SGUID id = "5EB31A63-F5C4-462F-9027-F9C86D98F49C"_schematyc_guid;						//5.3
																													
			//either way of providing the guid data is valid (with {enclosing} and without) in 5.4
			//(in 5.3 they are also valid, but you must be consistent. The two values are not identical, as it reads the leading brace as part of the guid)
			//static Schematyc::SGUID id = "{5EB31A63-F5C4-462F-9027-F9C86D98F49C}"_cry_guid;	// or _schematyc_guid

			return id;
		}

		// a function that we expose to schematyc for the purposes of demonstration
		int SampleFunctionExposedToSchematyc(const Schematyc::CSharedString cCharPtrInput, const float floatInput);

		// a dataType that we expose to schematyc for the purposes of demonstration
		enum ESampleDataTypeExposedToSchematyc
		{
			VALUE_0,
			VALUE_1,
			VALUE_DEFAULT,
			VALUE_MORE,
			VALUE_LAST
		};

		
		// a signal that we expose to schematyc for the purposes of demonstration
		// note that the ReflectType function for this class is defined as a static function in the .cpp, and not as a part of the struct.
		// It is perfectly legal to define ReflectType as a static member function of any struct/class if you choose to do so.
		struct SampleSignalExposedToSchematyc
		{
			SampleSignalExposedToSchematyc() {}
			SampleSignalExposedToSchematyc(Schematyc::CSharedString sharedStringData, ESampleDataTypeExposedToSchematyc enumData)
				: m_signalSharedStringData(sharedStringData),
				m_signalEnumData(enumData)
			{
			}

			Schematyc::CSharedString m_signalSharedStringData;
			ESampleDataTypeExposedToSchematyc m_signalEnumData;
		};

		// for this sampleSchematycComponent I will not be showing all usages available
		// but you can expose (EnvDataTypes), (EnvFunctions), (EnvSignals), EnvInterfaces, EnvModules, (EnvComponents), EnvClasses, EnvActions, (EnvPackages)
		// I encourage you to investigate (5.4) CryEngine/CryCommon/CrySchematyc/Env/Elements folder for the relevant macros
	private:
	};
}