#include "StdAfx.h"
#include "EntityTransformUtilComponent.h"
#include "StaticAutoRegistrar.h"	// for macro (unnecessary later, this is proper part of engine in 5.3)
#include <Schematyc/Entity/EntityClasses.h>	// for g_entityClassGUID (5.3)

#include <CrySchematyc\STDEnvAPI.h>		// for entity utils

namespace AugSchematycComponents
{
	namespace Utils
	{
		static void RegisterEntityTransformUtilComponent(Schematyc::IEnvRegistrar& registrar)
		{
			Schematyc::CEnvRegistrationScope scope = registrar.Scope(Schematyc::g_entityClassGUID);
			{
				auto pComponent = SCHEMATYC_MAKE_ENV_COMPONENT(augCEntityTransformUtilComponent, "EntityTransformUtil");
				pComponent->SetDescription("Some utility functions related to transforms for Schematyc");
				pComponent->SetIcon("icons:schematyc/entity_utils_component.ico");
				pComponent->SetAuthor("Augmea Inc.");
				pComponent->SetFlags({ Schematyc::EEnvComponentFlags::Singleton });

				scope.Register(pComponent);
				Schematyc::CEnvRegistrationScope componentScope = registrar.Scope(pComponent->GetGUID());
				//Functions
				{
					auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCEntityTransformUtilComponent::GetEntityTransform, "7AEA2BA5-1117-4F46-A82E-5BFB20F4FDD3"_schematyc_guid, "GetEntityTransform");
					pFunction->SetDescription("Get the transform of the entity");
					pFunction->BindOutput(0, 'tran', "Transform", "Transform");
					pFunction->BindInput(1, 'eid', "EntityId", "EntityId");
					pFunction->SetFlags({ Schematyc::EEnvFunctionFlags::Member });
					componentScope.Register(pFunction);

					pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCEntityTransformUtilComponent::RotationEquals, "0128BAD0-6B80-4317-8B41-7CA06791B137"_schematyc_guid, "RotationEquals");
					pFunction->SetDescription("Compare two transformations and see if they have equal rotations to within some epsilon (in degrees)");
					pFunction->BindOutput(0, 'bool', "Result", "Are these rotations equal");
					pFunction->BindInput(1, 'trn1', "A", "First transformation");
					pFunction->BindInput(2, 'trn2', "B", "Second transformation");
					pFunction->BindInput(3, 'eps', "Epsilon", "Epsilon (in degrees)", 5.f);
					pFunction->SetFlags({ Schematyc::EEnvFunctionFlags::Member });
					componentScope.Register(pFunction);

					pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCEntityTransformUtilComponent::EntityIdEquals, "860BAF6D-9271-48CA-94C9-8DE3E335F5B7"_schematyc_guid, "EntityIdEquals");
					pFunction->SetDescription("Compare two entityIds and see if they are equal");
					pFunction->BindOutput(0, 'bool', "Result", "Are these entityIds equal");
					pFunction->BindInput(1, 'eid1', "EntityId", "First entityId");
					pFunction->BindInput(2, 'eid2', "EntityId", "Second entityId");
					pFunction->SetFlags({ Schematyc::EEnvFunctionFlags::Member });
					componentScope.Register(pFunction);

					pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&augCEntityTransformUtilComponent::Rotate, "9F0DE7C0-9229-4E26-B840-AAE2BC75AC41"_schematyc_guid, "Rotate");
					pFunction->SetDescription("Rotate the entity");
					pFunction->BindInput(1, 'rot', "Rotation", "Rotation");
					pFunction->SetFlags({ Schematyc::EEnvFunctionFlags::Member });
					componentScope.Register(pFunction);
				}
			}
		}
		CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterEntityTransformUtilComponent);

		bool augCEntityTransformUtilComponent::RotationEquals(Schematyc::CTransform A, Schematyc::CTransform B, float epsilon)
		{
			Ang3 angA = A.GetRotation().ToAng3();
			Ang3 angB = B.GetRotation().ToAng3();

			epsilon = DEG2RAD(epsilon);

			return (angA.IsEquivalent(angB, epsilon));
		}

		bool augCEntityTransformUtilComponent::EntityIdEquals(Schematyc::ExplicitEntityId eid, Schematyc::ExplicitEntityId eid2)
		{
			return (static_cast<EntityId>(eid) == static_cast<EntityId>(eid2));
		}

		void augCEntityTransformUtilComponent::Rotate(Schematyc::CRotation rotation)
		{
			auto entity = &Schematyc::EntityUtils::GetEntity(*this);
			auto q = entity->GetRotation();

			q = q * rotation.ToQuat();

			entity->SetRotation(q);
		}
	}
}