// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 12.07.2017 by Luke Edwards 

#pragma once
#include <CryEntitySystem\IEntityComponent.h>
#include <CryEntitySystem\IEntity.h>

#include <CrySchematyc\CoreAPI.h>
#include <CrySchematyc\STDEnvAPI.h>
#include <Schematyc\ISTDEnv.h>
#include <Schematyc\Types\BasicTypes.h>

#include "Utils\ComponentSelector.h"
#include "IAttachPointIgnoreGeomComponent.h"

namespace AugSchematycComponents
{
	class augCAttachPointComponent : public Schematyc::CComponent
	{
	//PROPERTIES
	public:
		struct SAttachDirection
		{
			SAttachDirection()
				: direction(1.f)
				, bBidirectional(false)
			{ }

			static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SAttachDirection>& desc)
			{
				desc.SetGUID("471E2B57-97C0-4A85-BA5B-1DF8978B5195"_schematyc_guid);	//5.3
				desc.AddMember(&SAttachDirection::direction, 'dir', "Direction", "Direction to attach");
				desc.AddMember(&SAttachDirection::bBidirectional, 'bi', "Bidirectional", "Is direction valid from both sides?");
		
				return desc.GetGUID();
			}

			void Serialize(Serialization::IArchive& archive);
			
			Vec3 direction;
			bool bBidirectional;
		};

		struct SAttachPointData
		{
			SAttachPointData() 
			:
				mountingPoint(1.f),
				mountPointSize(1.f),
				attachPointRange(1.f),
				bThisWillAttach(false),
				objId(Schematyc::ObjectId::Invalid)
			{
			}
			SAttachPointData(const Vec3 mountPoint, const SAttachDirection attachDir, float size = 1.f, float range = 1.f, bool willAttach = false, const Schematyc::ObjectId id = Schematyc::ObjectId::Invalid)
				: mountingPoint(mountPoint),
				attachDirection(attachDir),
				mountPointSize(size),
				attachPointRange(range),
				bThisWillAttach(willAttach),
				objId(id)
			{
			}
			
			Vec3 mountingPoint;
			SAttachDirection attachDirection;
			float mountPointSize;
			float attachPointRange;
			bool bThisWillAttach;

			Schematyc::ObjectId objId;

			bool ignoreGeometry;
			bool useDefaultGeomComponent;
			std::shared_ptr<IAttachPointIgnoreGeomComponent> ignoreGeomComponent;
			//std::vector<Schematyc::CSharedString> ignoredGeometryNames;
			Schematyc::CSharedString ignoreGeomComponentGUIDString;
			string _ignoreGeomComponentGUIDString;
			//Schematyc::SerializationUtils::Container<std::vector<Schematyc::CSharedString>, 

			static inline Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SAttachPointData>& desc)	//5.3
			{
				desc.SetGUID("EF8F08E4-1268-4A82-8C77-3A32E1BF858B"_schematyc_guid);
		
				desc.AddMember(&SAttachPointData::mountingPoint, 'pMnt', "MountPoint", "Mounting point location");
				desc.AddMember(&SAttachPointData::attachDirection, 'aDir', "Direction", "Valid attach direction");
				desc.AddMember(&SAttachPointData::mountPointSize, 'mps', "MountPointSize", "Size of mount point");
				desc.AddMember(&SAttachPointData::attachPointRange, 'apr', "AttachPointRange", "Range to search from attach point for potential attachments");
				desc.AddMember(&SAttachPointData::bThisWillAttach, 'watt', "WillAttach", "This object will attach to another");
				desc.AddMember(&SAttachPointData::objId, 'obj', "ObjectId", "ObjectId");
				desc.AddMember(&SAttachPointData::ignoreGeometry, 'bIgG', "bIgnoreGeom", "Should ignore some geometry from this entity when attached.");
				desc.AddMember(&SAttachPointData::useDefaultGeomComponent, 'dGeo', "useDefaultGeomComponent", "Whetehr or not the geometry we ignore comes from a default schematyc geom component.");
				desc.AddMember(&SAttachPointData::ignoreGeomComponent, 'cGeo', "IgnGeomComponent", "The non-default geometry component to use when ignoring the geometry for the attach point constraint.");
				//desc.AddMember(&SAttachPointData::ignoredGeometryNames, 'ignr', "IgnPartNames", "Name of parts to ignore when attaching objects");
				desc.AddMember(&SAttachPointData::ignoreGeomComponentGUIDString, 'guid', "IgnGeomGUID", "GUID of the geometry component which we want to select parts to ignore from.");

				return desc.GetGUID();
			}

			void Serialize(Serialization::IArchive& archive);
		};

	private:
		struct SProperties
		{
			SProperties();
			~SProperties();

			void Serialize(Serialization::IArchive& archive);

			SAttachPointData attachPointData;
			//SerializationUtils::CComponentSelector<IAttachPointIgnoreGeomComponent> componentSelector;
			SerializationUtils::ICommonComponentSelectorPtr commonComponentSelector;// = std::make_shared<SerializationUtils::CComponentSelector<IAttachPointIgnoreGeomComponent>>();
		};
	//~PROPERTIES

	//PREVIEWER
	private:

		struct SPreviewProperties
		{
			void Serialize(Serialization::IArchive& archive);

			bool bShowMountPoints = false;
			bool bShowAttachDirections = false;
		};

		class CPreviewer : public Schematyc::IComponentPreviewer
		{
		public:

			// IComponentPreviewer
			virtual void SerializeProperties(Serialization::IArchive& archive) override;
			virtual void Render(const Schematyc::IObject& object, const Schematyc::CComponent& component, const SRendParams& params, const SRenderingPassInfo& passInfo) const override;
			// ~IComponentPreviewer

		private:

			SPreviewProperties m_properties;
		};
	//~PREVIEWER

	public:
		augCAttachPointComponent()
		{
			const SProperties* pProperties = static_cast<const SProperties*>(Schematyc::CComponent::GetProperties());
			//if (pProperties->validAttachDirections)
			if (pProperties)
			{
				Init();
			}
		}
		virtual ~augCAttachPointComponent() 
		{
			attachPointMap.erase(Schematyc::CComponent::GetObject().GetId());
			idMap.erase(Schematyc::EntityUtils::GetEntity(*this).GetId());
		}

		static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<augCAttachPointComponent>& desc)
		{
			desc.SetGUID(IID());

			return IID();
		}

		static Schematyc::SGUID IID()
		{
			static Schematyc::SGUID id = "F7534372-55B9-4981-A7D2-794DD04EE859"_schematyc_guid;

			return id;
		}

		static void Register(Schematyc::IEnvRegistrar& registrar);

		virtual bool Init() override;

		//Functions
		Vec3 GetMountPoint(SAttachPointData apd) const;
		Vec3 GetAttachDirection(SAttachPointData apd) const;
		Vec3 GetAttachLocation(SAttachPointData apd) const;
		bool WillAttachTo(SAttachPointData apd) const;
		bool WillReceiveAttachment(SAttachPointData apd) const;
		bool IsInRangeOfOther(Schematyc::ExplicitEntityId other) const;
		bool IsInValidDirectionOfOther(Schematyc::ExplicitEntityId other) const;
		bool Attach(Schematyc::ExplicitEntityId other, const bool attach = true);

		static const SAttachPointData GetAttachPointData(Schematyc::ExplicitEntityId other);
		void SendAttachPointDataSignal(Schematyc::ExplicitEntityId entityId) const;

		//Signal
		struct AttachPointDataSignal
		{
			AttachPointDataSignal() {}
			AttachPointDataSignal(SAttachPointData apd, Schematyc::ExplicitEntityId eid)
				: attachPointData(apd),
				entityId(eid)
			{
			}

			SAttachPointData attachPointData;
			Schematyc::ExplicitEntityId entityId;

			static inline Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<AttachPointDataSignal>& desc)	//5.3
			{
				desc.SetGUID("547C2D1E-41B2-4CA8-8A78-09C8D35B6C56"_schematyc_guid);
						
				desc.AddMember(&AttachPointDataSignal::attachPointData, 'apd', "AttachPointData", "AttachPointData");
				desc.AddMember(&AttachPointDataSignal::entityId, 'eid', "EntityId", "EntityId");

				return desc.GetGUID();
			}
		};

		
		//static std::map <EntityId, SAttachPointData> attachPointMap;
		static std::map <Schematyc::ObjectId, SAttachPointData> attachPointMap;
		static std::map <EntityId, Schematyc::ObjectId> idMap;
		//static std::map <EntityId, Schematyc::ObjectId> idMap;

	private:
		void RenderPoint() const;
		void RenderAttachDirection(const SAttachDirection& dir) const;

		IEntity* m_pAttachedEntity;
	};
}