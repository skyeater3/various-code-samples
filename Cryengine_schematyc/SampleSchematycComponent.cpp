// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 07.07.2017 by Luke Edwards 

#include "StdAfx.h"
#include "SampleSchematycComponent.h"
#include "StaticAutoRegistrar.h"	// for macro (unnecessary later, this is proper part of engine in 5.3)
#include <Schematyc/Entity/EntityClasses.h>	// for g_entityClassGUID (5.3)

using SampleSignal = AugSample::CSampleSchematycComponent::SampleSignalExposedToSchematyc;
using ESampleData = AugSample::CSampleSchematycComponent::ESampleDataTypeExposedToSchematyc;

namespace AugSample
{
	// not a member function of the signal struct
	//static inline CryGUID ReflectType(Schematyc::CTypeDesc<SampleSignal>& desc)					//5.4
	static inline Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SampleSignal>& desc)	//5.3
	{
		//desc.SetGUID("{A78EFACA-3D63-4A29-9958-2BF17F7EFA53}"_cry_guid);		// 5.4
		desc.SetGUID("A78EFACA-3D63-4A29-9958-2BF17F7EFA53"_schematyc_guid);	// 5.3
		
		desc.AddMember(&SampleSignal::m_signalEnumData, 'enum', "Enum", "Sample enum data");			// up to 4 char 'name' just any meaningful series of characters to represent unique data
		desc.AddMember(&SampleSignal::m_signalSharedStringData, 'str', "String", "Sample shared string data");

		return desc.GetGUID();
	}

	// This is not really necessary in 5.3, but is the easiest way to do it in 5.4 (avoiding the need for the Serialization_enum_ Macros that follow, and the Schematyc_make_env_data_type macro section in Register (below)
	//static inline CryGUID ReflectType(Schematyc::CTypeDesc<ESampleData>& desc)					//5.4
	static inline Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<ESampleData>& desc)	//5.3
	{
		//desc.SetGUID("{BA56EB07-207D-4C31-A4F0-C0D8AD6492D5}"_cry_guid);		//5.4
		desc.SetGUID("BA56EB07-207D-4C31-A4F0-C0D8AD6492D5"_schematyc_guid);	//5.3
		//desc.SetLabel("Sample Data Enum (5.4)"); // 5.4
		//desc.SetDescription("Simple enum data type exposed to schematyc for demonstration purposes"); // 5.4
		desc.AddConstant(ESampleData::VALUE_0, "Val0", "0");
		desc.AddConstant(ESampleData::VALUE_1, "Val1", "1");
		desc.AddConstant(ESampleData::VALUE_DEFAULT, "ValDefault", "Default");
		desc.AddConstant(ESampleData::VALUE_MORE, "ValMore", "More");
		desc.AddConstant(ESampleData::VALUE_LAST, "ValLast", "Last");
		
		// super easy to set default value, and this whole data type in 5.4
		//desc.SetDefaultValue(ESampleData::VALUE_DEFAULT); // in 5.3 this is done along with the MAKE_ENV_DATATYPE macro found below
		
		return desc.GetGUID();
	}

	static void RegisterSampleSchematycComponent(Schematyc::IEnvRegistrar& registrar)
	{
		//Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());		//5.4
		Schematyc::CEnvRegistrationScope scope = registrar.Scope(Schematyc::g_entityClassGUID);
		{
			//auto pComponent = SCHEMATYC_MAKE_ENV_COMPONENT(CSampleSchematycComponent);	//5.4
			auto pComponent = SCHEMATYC_MAKE_ENV_COMPONENT(CSampleSchematycComponent, "SampleComponent");
			pComponent->SetDescription("Sample schematyc component for demonstration purposes");
			pComponent->SetIcon("icons:Game/Game_Play.ico");					// this pushed to CTypeDesc parameter of ReflectType function in 5.4
			pComponent->SetAuthor("Augmea Inc.");								// think this gets pushed to CTypeDesc as well
			pComponent->SetFlags(Schematyc::EEnvComponentFlags::Singleton);		// there are different flags available when 5.4 hits such as Member, Construction
										// I think all of the flags available here in 5.3 are pushed to CTypeDesc for 5.4 as ComponentFlags (see ReflectType in .h for more details)
			
			//Schematyc::CEnvRegistrationScope componentScope = scope.Register(pComponent);	//5.4
			scope.Register(pComponent);
			Schematyc::CEnvRegistrationScope componentScope = registrar.Scope(pComponent->GetGUID());
			//Functions
			{		
				//auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CSampleSchematycComponent::SampleFunctionExposedToSchematyc, "{C5FAFFC7-5A30-4565-96C0-8E5601281359}"_cry_guid, "SampleFunction");	//5.4
				auto pFunction = SCHEMATYC_MAKE_ENV_FUNCTION(&CSampleSchematycComponent::SampleFunctionExposedToSchematyc, "C5FAFFC7-5A30-4565-96C0-8E5601281359"_schematyc_guid, "SampleFunction");
				pFunction->SetDescription("This is a sample function for demonstration purposes");
				pFunction->BindInput(1, 'str', "String", "A sample string input value", Schematyc::CSharedString("DefaultString"));			// inputs start counting from 1
				pFunction->BindInput(2, 'flt', "Float", "A sample float input value", float(69.f));
				pFunction->BindOutput(0, 'int', "Meaning", "The answer to the ultimate question of life, the universe, and everything");	// output is 0
				componentScope.Register(pFunction);
			}
			//Signals
			{
				//auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL(CSampleSchematycComponent::SampleSignalExposedToSchematyc);	//5.4
				auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL_TYPE(CSampleSchematycComponent::SampleSignalExposedToSchematyc, "SampleSignal");	// note change in macro name - macro with matching name in 5.3 takes guid as first parameter
				pSignal->SetDescription("This is a sample signal for demonstration purposes, sent whenever the sample function is run");
				pSignal->SetAuthor("Augmea Inc.");
				componentScope.Register(pSignal);
			}
			//Datatypes
			{
				// This whole chunk replaced in 5.4 by the RegisterType call near the top of this file
				auto pDataType = SCHEMATYC_MAKE_ENV_DATA_TYPE(ESampleData, "SampleDataType");
				pDataType->SetDescription("This is a sample data type which has been exposed to schematyc for demonstration purposes");
				pDataType->SetDefaultValue(ESampleData::VALUE_DEFAULT);
				pDataType->SetAuthor("Augmea Inc.");
				componentScope.Register(pDataType);
			}
		}
	}

	// definition of our sample function
	// the only thing demonstrated here is the ProcessSignal call.
	int CSampleSchematycComponent::SampleFunctionExposedToSchematyc(const Schematyc::CSharedString sharedStringInput, const float floatInput)
	{
		// do something with incoming data
		// lets forward it to the signal, and send it
		int val = crymath::abs((int) floatInput);
		if (val > (int) ESampleData::VALUE_LAST)
		{
			val = (int) floatInput % (int)ESampleData::VALUE_LAST;
		}

		SampleSignal signal = SampleSignal(sharedStringInput, (ESampleData) val);
		
		//m_pEntity->GetSchematycObject()->ProcessSignal(signal, GetGUID());	//5.4, optional guid that lets the signal receiver know who sent it.
		CComponent::GetObject().ProcessSignal(signal);

		//return meaningful value
		return 42;
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterSampleSchematycComponent);
}