#pragma once
#include <CrySchematyc\CoreAPI.h>
#include <Schematyc\Types\BasicTypes.h>

namespace AugSchematycComponents
{

	class augCPickerListener : public Schematyc::CComponent
	{
	private:
		struct SPickerSelectSignal
		{
			SPickerSelectSignal() {};
			SPickerSelectSignal(EntityId _entityId) :
				entityId(static_cast<Schematyc::ExplicitEntityId>(_entityId))
			{
			};

			static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SPickerSelectSignal>& typeInfo)
			{
				typeInfo.SetGUID("076D9984-ABB9-4670-A964-3F89D52697D4"_schematyc_guid);
				typeInfo.AddMember(&SPickerSelectSignal::entityId, 'eid', "EntityId", "Entity Id of picked entity.");
				return typeInfo.GetGUID();
			};

			Schematyc::ExplicitEntityId entityId;
		};

		struct SPickerDeselectSignal
		{
			SPickerDeselectSignal() {};
			SPickerDeselectSignal(EntityId _entityId) :
				entityId(static_cast<Schematyc::ExplicitEntityId>(_entityId))
			{
			};

			static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SPickerDeselectSignal>& typeInfo)
			{
				typeInfo.SetGUID("C8AA3412-3E11-4706-9D60-552AA1DB8CBC"_schematyc_guid);
				typeInfo.AddMember(&SPickerDeselectSignal::entityId, 'eid', "EntityId", "Entity Id of unpicked entity.");
				return typeInfo.GetGUID();
			};

			Schematyc::ExplicitEntityId entityId;
		};

		//struct SPickerInteractSignal
		//{
		//	SPickerInteractSignal() {};
		//	SPickerInteractSignal(EntityId _entityId) :
		//		entityId(static_cast<Schematyc::ExplicitEntityId>(_entityId))
		//	{
		//	};

		//	static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SPickerInteractSignal>& typeInfo)
		//	{
		//		typeInfo.SetGUID("9B0EBBCB-6C5E-4A62-99D6-30CF6A9EB086"_schematyc_guid);
		//		typeInfo.AddMember(&SPickerInteractSignal::entityId, 'eid', "EntityId", "Entity Id of entity to interact with.");
		//		return typeInfo.GetGUID();
		//	};

		//	Schematyc::ExplicitEntityId entityId;
		//};

	public:
		enum EPickerEventType
		{
			PICK_ENTITY,
			UNPICK_ENTITY,
			//INTERACT_WITH_ENTITY
		};
	
		// Schematyc::CComponent
		virtual bool Init() override;
		// ~Schematyc::CComponent	

		static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<augCPickerListener>& typeInfo);
		static void             RegisterPickerListenerComponent(Schematyc::IEnvRegistrar& registrar);

		void PickerEvent(EntityId entityId, EPickerEventType pickEventType);
	};
}