While investigating what changes would be required to upgrade one of our projects to the next version of CryEngine (5.4 was still in preview), I performed an exploration of the Schematyc system for CryEngine (similar to Unreal Engine Blueprints).

This became a core component of CryEngine in 5.4 - but was also accessible, with a little bit of work, through 5.3

Some of these files were written to highlight changes or requirements for other developers, to use as a blueprint.

This code isn't strictly gameplay related - but it does show off some useage of lambdas and pointers, working with a larger code-base, working to an API, and coding style.

### Included in this package are:

_Plugin_ - highlighting expected changes to CryEngine plugin code required for 5.4 (and schematyc linkage)

_**SampleSchematycComponent**_ folder:

_SampleSchematycComponent_ - sample schematyc component, showing off (then current) useage in 5.3 and expected useage in 5.4

_**SchematycComponents**_ folder:

_PickerListener_ - A very simple component that hooks into a signal from an Object Picker and forwards it into Schematyc

_MeshExplodeComponent_ - A conversion of an existing effect into a schematyc component to demonstrate where changes might be necessary while Schematyc was still in Beta (and to demo an editor dropdown box for finding schematyc components implementing a given interface - in this case IAttachPointIgnoreGeomComponent). This effect took a geometry file and split it into lots of pieces and radially 'exploded' the mesh so that the model appeared to break up into individual screws and springs and sheets of metal

_AttachPointComponent_ - A schematyc component that was used to designate the location(s) on an object from which it could connect to another object with  complementary attachpoint component(s). Eg: plugging a  power cable into a socket (and maybe choosing to enforce rotational restrictions)

_IAttachPointIgnoreGeomComponent_ - Was used as a kind of Schematyc component Interface - to allow me to select a non-standard geometry component (the mesh explode component) as a target for ignoring by other geometry (because the geometry change was causing all sorts of unwanted physics interactions)

_**SchematycComponents/Utils**_ folder:

_ResourceTypes_ - custom serialization method for selecting a component with some type defined in code

_ICommonComponentSelector_ - interface which will allow selection of schematyc components which implement an interface - by checking to make sure the schematyc object is of the correct type

_EntityTransformUtilComponent_ - a schematyc component with some simple utility functions [GetTransform (declared in header - just to show it doesn't matter), Rotate, RotateEquals, EntityIdEquals]

_ComponentSelector_ - implementation of ICommonComponentSelector
