#pragma once
#include <CryEntitySystem\IEntityComponent.h>
#include <CryEntitySystem\IEntity.h>

#include <CrySchematyc\CoreAPI.h>
#include <CrySchematyc\STDEnvAPI.h>
#include <Schematyc\ISTDEnv.h>
#include <Schematyc\Types\BasicTypes.h>

#include "StaticAutoRegistrar.h"	// for macro (unnecessary later, this is proper part of engine in 5.3)
#include <Schematyc/Entity/EntityClasses.h>	// for g_entityClassGUID (5.3)


namespace AugSchematycComponents
{
	class IAttachPointIgnoreGeomComponent : public Schematyc::CComponent, IEntityComponent
	{
		CRY_ENTITY_COMPONENT_INTERFACE(IAttachPointIgnoreGeomComponent, 0x23BA3D6B83E84B11, 0x91E6EB2B22FB1F02)

	public:
		IAttachPointIgnoreGeomComponent() = default;
		virtual ~IAttachPointIgnoreGeomComponent() {};

		static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<IAttachPointIgnoreGeomComponent>& desc)
		{
			desc.SetGUID(Schematyc_IID());

			return Schematyc_IID();
		}

		static Schematyc::SGUID Schematyc_IID()
		{
			static Schematyc::SGUID id = "867797F8-7DA7-470F-943B-B5CB48DC4505"_schematyc_guid;

			return id;
		}

		void Serialize(Serialization::IArchive & archive)
		{
			
		}

		//static void Register(Schematyc::IEnvRegistrar& registrar);
		// pass component scope into this register function when the interface is declared
		static void Register(Schematyc::CEnvRegistrationScope& scope)
		{
			auto pInterface = SCHEMATYC_MAKE_ENV_INTERFACE(Schematyc_IID(), "IAttachPointIgnoreGeomComponent");
			// registering in the component scope automatically sets this parent to the component
			//pInterface->SetParent(parent.get());
			scope.Register(pInterface);
		}
		//virtual bool Init() override;
	};

	static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<std::shared_ptr<IAttachPointIgnoreGeomComponent>>& desc)
	{
		desc.SetGUID("{781AB059-E3AD-4FB0-A464-B7A27BEA22C5}"_schematyc_guid);
		return desc.GetGUID();
	}

	static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<std::vector<Schematyc::CSharedString>>& desc)
	{
		desc.SetGUID("B31DE13E-73D2-4497-B578-B88B1D59EB57"_schematyc_guid);
		return desc.GetGUID();
	}
}

