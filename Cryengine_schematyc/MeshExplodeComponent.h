#pragma once
#include <CrySchematyc\CoreAPI.h>
#include <Schematyc\Types\BasicTypes.h>

#include "ExplodeMesh.h"	// lazy

#include "CryInput\IHardwareMouse.h"
#include "CrySystem\ISystem.h"
#include "CryInput\IInput.h"
#include "CrySystem\IWindowMessageHandler.h"

#include "IAttachPointIgnoreGeomComponent.h"

namespace AugSchematycComponents
{
	class augCMeshExplodeComponent : /*public Schematyc::CComponent//*/ public IAttachPointIgnoreGeomComponent
	{
	private:
		struct SExplodeSignal
		{
			SExplodeSignal() {};
			SExplodeSignal(EntityId _entityId) :
				entityId(static_cast<Schematyc::ExplicitEntityId>(_entityId))
			{
			};

			static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<SExplodeSignal>& typeInfo)
			{
				typeInfo.SetGUID("009DF593-5E04-4C19-9EF1-EE656FE3FC8D"_schematyc_guid);
				typeInfo.AddMember(&SExplodeSignal::entityId, 'eid', "EntityId", "Entity Id of exploding entity.");
				return typeInfo.GetGUID();
			};

			Schematyc::ExplicitEntityId entityId;
		};

		struct SProperties
		{
			SProperties();

			void Serialize(Serialization::IArchive& archive);

			string m_fileName;
			float m_speed = 1.0f;
			float x = 0.f, y = 0.f, z = 0.f;
			bool m_physicalize = false;
			bool m_run = true;
			bool m_explode = false;
		};

		private:
			
		class CPreviewer : public Schematyc::IComponentPreviewer
		{
		public:

			// IComponentPreviewer
			virtual void SerializeProperties(Serialization::IArchive& archive) override {};
			virtual void Render(const Schematyc::IObject& object, const Schematyc::CComponent& component, const SRendParams& params, const SRenderingPassInfo& passInfo) const override;
		};
	//~PREVIEWER

	public:	
		augCMeshExplodeComponent() = default;
		virtual ~augCMeshExplodeComponent() {}

		// Schematyc::CComponent
		virtual bool Init() override;
		virtual void Run(Schematyc::ESimulationMode mode) override;
		virtual void Shutdown() override;
		// ~Schematyc::CComponent	

		static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<augCMeshExplodeComponent>& typeInfo);
		static void             RegisterMeshExplodeComponent(Schematyc::IEnvRegistrar& registrar);

	protected:
		void Reset(int resetEvent);
		
		void Update(const Schematyc::SUpdateContext& updateContext);
		void PrephysicsUpdate(const Schematyc::SUpdateContext& updateContext);

		void RecurseStatObj(IStatObj* statObj);
		void RemoveEntities();
		void InitEntities();
		void EnablePhysics(bool enable);
		IPhysicalEntity* Physicalize(SubPart* part, pe_type type);
		void SetState(int state);
		IGeometry* CreateMesh(IGeomManager* pGeoman, IStatObj* statObj, int MatId);

		int m_slot;
		Vec3 m_explodeOrigin;
		float t;
		bool done;

		bool gameRunning;
		bool physicsEnabled;
		enum state { open, close, transition };
		state targetState;
		state currentState;
		IStatObj* OBJ;

	protected:
		std::vector<QuatT> origin;
		std::vector<QuatT> target;
		std::vector<QuatT> last;
		std::vector<SubPart*> entities;

		Schematyc::CConnectionScope m_connectionScope;

		// Inherited via ISystemEventListener
		//virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	};
}