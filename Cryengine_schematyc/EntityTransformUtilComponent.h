// Copyright 2017 Augmea Inc. All rights reserved.
// Created: 12.07.2017 by Luke Edwards 

#pragma once
#include <CryEntitySystem\IEntityComponent.h>
#include <CryEntitySystem\IEntity.h>

#include <CrySchematyc\CoreAPI.h>
#include <Schematyc\Types\BasicTypes.h>

namespace AugSchematycComponents
{
	namespace Utils
	{
		class augCEntityTransformUtilComponent : public Schematyc::CComponent
		{
		public:
			augCEntityTransformUtilComponent() = default;
			virtual ~augCEntityTransformUtilComponent() {}

			static Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<augCEntityTransformUtilComponent>& desc)
			{
				desc.SetGUID(IID());

				return IID();
			}

			static Schematyc::SGUID IID()
			{
				static Schematyc::SGUID id = "01A9C041-C1EF-4B25-975E-80E1C199D951"_schematyc_guid;

				return id;
			}

			//Functions
			// Gets the transform of the entity with id
			Schematyc::CTransform GetEntityTransform(Schematyc::ExplicitEntityId id)
			{
				auto transform = gEnv->pEntitySystem->GetEntity((EntityId)id)->GetWorldTM();
				auto result = Schematyc::CTransform(transform);
				return result;
			}

			// Check to see if two rotations are equal to within some epsilon (provided in degrees)
			bool RotationEquals(Schematyc::CTransform A, Schematyc::CTransform B, float epsilon);
			bool EntityIdEquals(Schematyc::ExplicitEntityId eid, Schematyc::ExplicitEntityId eid2);
			void Rotate(Schematyc::CRotation rotation);
		};
	}
}