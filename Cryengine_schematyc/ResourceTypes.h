#pragma once

#include <CrySerialization\Decorators\ResourceSelector.h>
#include <Schematyc\Types\ResourceTypes.h>
#include <Schematyc\Utils\GUID.h>
#include <Schematyc\Reflection\Reflection.h>

#include "ComponentSelector.h"
#include "ICommonComponentSelector.h"

namespace Schematyc
{
	class CComponent;
}
namespace AugSchematycComponents
{
	namespace SerializationUtils
	{	
		// going to define a custom serialization method for selecting a component with some type defined in code

		// ComponentResourceSelector is just an impl of IResourceSelector
		// But because it is a custom impl it is easy to differentiate from the standard ResourceSelector<string> (although they do exactly the same things)
		template<class string>
		struct ComponentResourceSelector : Serialization::IResourceSelector
		{
			string& value;

			const char*           GetValue() const        { return value.c_str(); }
			void                  SetValue(const char* s) { value = s; }
			const void*           GetHandle() const       { return &value; }
			Serialization::TypeID GetType() const         { return Serialization::TypeID::get<string>(); }

			ComponentResourceSelector(string& value, const char* resourceType, const Serialization::ICustomResourceParamsPtr& pCustomParams = Serialization::ICustomResourceParamsPtr())
				: value(value)
			{
				this->resourceType = resourceType;
				this->pCustomParams = pCustomParams;
			}
		};

		// and this is our custom serialization method.
		// displays the names of the components matching the type we want to select for, and stores the guid behind the scenes
		template<class string>
		bool Serialize (Serialization::IArchive& ar, ComponentResourceSelector<string>& value, const char* name, const char* label)
		{
			// with this impl we no longer actually need the Editor plugin dll_string function
			ICommonComponentSelector* ccs = static_cast<ICommonComponentSelector*>(value.pCustomParams.get());
			std::vector<std::string> names, guids;
			ccs->GetBases(names, guids);

			assert(names.size() == guids.size());

			Serialization::StringListStatic slsNames, slsGuids;
			for (int i = 0; i < names.size();  i++)
			{
				slsNames.emplace_back(names[i].c_str());
				slsGuids.emplace_back(guids[i].c_str());
			}


			// impl for this appropriated from Enum.cpp serialization technique
			// basically wanted to create one list that we display then use that to inform the value behind the scenes (see 'name' on screen, store 'guid' in value)
			int index = -1;
			if (ar.isOutput())
			{
				index = slsGuids.find(value.value);
			}

			yasli::StringListStaticValue stringListName(/*ar.isEdit() ? slsNames : slsGuids*/ slsNames, index);
			if (!ar(stringListName, name, label))
				return false;

			if (ar.isInput())
			{
				if (stringListName.index() == yasli::StringListStatic::npos) 
				{
					ar.error(value.value, "Unable to load unregistered enumeration value of %s (label %s).", name, label);
					return false;
				}
				value.SetValue(/*ar.isEdit() ? stringListName.c_str() : stringListName.c_str()*/ slsGuids.at(slsNames.find(stringListName.c_str())));
			}
			return true;
		}

		inline ComponentResourceSelector<string> ComponentInterfaceName(string& value, ICommonComponentSelectorPtr &componentSelector)
		{
			return ComponentResourceSelector<string>(value, "ComponentInterface", componentSelector);
		}

		template<ComponentResourceSelector<string>(* SELECTOR)(string&, ICommonComponentSelectorPtr &)>
		struct SComponentInterfaceResourceNameSelector
		{
			inline SComponentInterfaceResourceNameSelector() {}

			inline SComponentInterfaceResourceNameSelector(const char* _szValue, ICommonComponentSelectorPtr _ptr)
				: value(_szValue), ptr(_ptr)
			{}

			inline bool operator==(const SComponentInterfaceResourceNameSelector& rhs) const
			{
				return value == rhs.value;
			}

			string value;
			ICommonComponentSelectorPtr ptr;
		};

		template<ComponentResourceSelector<string>(* SELECTOR)(string&, ICommonComponentSelectorPtr &componentSelector)> 
		inline bool Serialize(Serialization::IArchive& archive, SComponentInterfaceResourceNameSelector<SELECTOR>& value, const char* szName, const char* szLabel)
		{
			archive(SELECTOR(value.value, value.ptr), szName, szLabel);
			return true;
		}
	}

	using ComponentInterfaceName = SerializationUtils::SComponentInterfaceResourceNameSelector<&SerializationUtils::ComponentInterfaceName>;

	inline Schematyc::SGUID ReflectSchematycType(Schematyc::CTypeInfo<ComponentInterfaceName>& typeInfo)
	{
		typeInfo.DeclareSerializeable();
		return "441567D6-D452-4F10-8BA8-38DE3E0C7BE0"_schematyc_guid;
	}

}