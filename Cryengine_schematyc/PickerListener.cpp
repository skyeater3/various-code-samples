#pragma once

#include "StdAfx.h"
#include "PickerListener.h"
#include "StaticAutoRegistrar.h"	// for macro (unnecessary later, this is proper part of engine in 5.3)
#include "Schematyc\Entity\EntityClasses.h"

#include "Player\Picker\PlayerPicker.h"

namespace AugSchematycComponents
{
	bool augCPickerListener::Init()
	{
		auto itr = gEnv->pEntitySystem->GetEntityIterator();
		while (auto pEntity = itr->Next())
		{
			CPlayerPicker* playerPicker = pEntity->GetComponent<CPlayerPicker>();
			if (playerPicker != nullptr)
			{
				playerPicker->RegisterPickerListener(this);
				break;
			}
		}		

		return true;
	}
	Schematyc::SGUID augCPickerListener::ReflectSchematycType(Schematyc::CTypeInfo<augCPickerListener>& typeInfo)
	{
		typeInfo.SetGUID("B64EA52B-1B53-45CA-9179-2855B3DE76C9"_schematyc_guid);
		return typeInfo.GetGUID();
	}

	void augCPickerListener::RegisterPickerListenerComponent(Schematyc::IEnvRegistrar & registrar)
	{
		Schematyc::CEnvRegistrationScope scope = registrar.Scope(Schematyc::g_entityClassGUID);
		{
			auto pComponent = SCHEMATYC_MAKE_ENV_COMPONENT(augCPickerListener, "PickerListener");
			pComponent->SetAuthor("Augmea Inc.");
			pComponent->SetDescription("Listener for picker events");
			pComponent->SetIcon("icons:schematyc/entity_sensor_volume_component.ico");
			pComponent->SetFlags({ Schematyc::EEnvComponentFlags::Singleton });
			//pComponent->SetProperties(SProperties());
			//pComponent->SetPreviewer(CPreviewer());
			scope.Register(pComponent);

			Schematyc::CEnvRegistrationScope componentScope = registrar.Scope(pComponent->GetGUID());
			{
				auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL_TYPE(SPickerSelectSignal, "Picked");
				pSignal->SetAuthor("Augmea Inc.");
				pSignal->SetDescription("Sent when an entity is picked.");
				componentScope.Register(pSignal);
			}
			{
				auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL_TYPE(SPickerDeselectSignal, "Unpicked");
				pSignal->SetAuthor("Augmea Inc.");
				pSignal->SetDescription("Sent when an entity is unpicked.");
				componentScope.Register(pSignal);
			}
			//{
			//	auto pSignal = SCHEMATYC_MAKE_ENV_SIGNAL_TYPE(SPickerInteractSignal, "Interact");
			//	pSignal->SetAuthor("Augmea Inc.");
			//	pSignal->SetDescription("Sent when a picked entity attempts to interact.");
			//	componentScope.Register(pSignal);
			//}
		}
	}

	void augCPickerListener::PickerEvent(EntityId entityId, EPickerEventType pickEventType)
	{
		// Deliberately kept really simple. All it does is receive an event and forward it to Schematyc signal.
		// this doesn't care about the mechanics of picking (left to the picker)
		// or what you do with the information about what has been picked (left to the end user)
		switch (pickEventType)
		{
			case EPickerEventType::PICK_ENTITY:
				CComponent::GetObject().ProcessSignal(SPickerSelectSignal(entityId));
				break;
			case EPickerEventType::UNPICK_ENTITY:
				CComponent::GetObject().ProcessSignal(SPickerDeselectSignal(entityId));
				break;
			//case EPickerEventType::INTERACT_WITH_ENTITY:
			//	CComponent::GetObject().ProcessSignal(SPickerInteractSignal(entityId));
			//	break;
			default:
				break;
		}
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&augCPickerListener::RegisterPickerListenerComponent);
}
