**C++ CryEngine** *CryEngine_Schematyc* : Exploration of CryEngine 5.4 Schematyc system performed for benefit of the team before release of the Schematyc system from beta.


**C# Unity Meta 2 AR** *TutorialDisplay+Interface* : Customisable tutorial display for meta - Including editor files - and a way to select all GameObjects which implement a given interface in unity editor


**C# Unity** *Gauss_Kalman* : Custom Gaussian Smoothing implementation, and working with OpenCVSharp Kalman Filter


**C# Unity HoloLens Meta 2 AR** *Highlight* : Highlight for gazing at a building code used for both builds


**C# Unity HoloLens AR** *HoloControls* : Very simple Physical  Control examples - buttons, sliders, knobs

