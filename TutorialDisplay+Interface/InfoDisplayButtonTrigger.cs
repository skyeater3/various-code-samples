using Meta.HandInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Meta.HandInput.HandTrigger))]
[RequireComponent(typeof(Collider))]
public class InfoDisplayButtonTrigger : MonoBehaviour, IInfoDisplayTrigger
{
	public event EventHandler<InfoDisplayTriggerArgs> Triggered;

	private HandTrigger handTrigger;

	// Awake
	//void Awake()
	//{
		
	//}

	// Use this for initialization
	void Start() 
	{
		handTrigger = GetComponent<HandTrigger>();
        
		handTrigger.HandFeatureEnterEvent.AddListener(OnHandFeatureTrigger);
	}

	private void OnHandFeatureTrigger(HandFeature arg0)
	{
        if (arg0 is TopHandFeature)
        {
            Debug.Log("On Hand Feature Trigger");
            Triggered?.Invoke(this, new InfoDisplayTriggerArgs(true));
        }
	}

	void OnDestroy()
	{
		handTrigger.HandFeatureEnterEvent.RemoveListener(OnHandFeatureTrigger);
		handTrigger = null;
	}

	// Update is called once per frame
	//void Update() 
	//{
		
	//}
	
}
