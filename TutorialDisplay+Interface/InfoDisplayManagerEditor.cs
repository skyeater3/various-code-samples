using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Linq;

namespace Augmea
{
    [CustomEditor(typeof(InfoDisplayManager))]
    public class InfoDisplayManagerEditor : Editor
    {
        private ReorderableList infoSequences;

        private SerializedProperty skipScreenTrigger;
        private SerializedProperty skipSequenceTrigger;
        private SerializedProperty restartScreenTrigger;
        private SerializedProperty restartSequenceTrigger;
        private SerializedProperty closeSequenceTrigger;
        private SerializedProperty startInfoDisplayTrigger;

        private void OnEnable()
        {
            serializedObject.Update();

            infoSequences = new ReorderableList(serializedObject, serializedObject.FindProperty("infoSequences"), true, true, true, true);
            infoSequences.drawElementCallback = DrawElementCallback;
            infoSequences.elementHeightCallback = ElementHeightCallback;
            infoSequences.drawHeaderCallback = DrawHeaderCallback;
            infoSequences.drawElementBackgroundCallback = DrawElementBackgroundCallback;

            skipScreenTrigger = serializedObject.FindProperty("skipScreenTrigger");
            skipSequenceTrigger = serializedObject.FindProperty("skipSequenceTrigger");
            restartScreenTrigger = serializedObject.FindProperty("restartScreenTrigger");
            restartSequenceTrigger = serializedObject.FindProperty("restartSequenceTrigger");
            closeSequenceTrigger = serializedObject.FindProperty("closeSequenceTrigger");
            startInfoDisplayTrigger = serializedObject.FindProperty("startInfoDisplayTrigger");
        }

        private void DrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, "Info Sequences");
        }

        private float ElementHeightCallback(int index)
        {
            //Repaint();
            //return (EditorGUIUtility.singleLineHeight) + 2;
            
            if (index > -1 && infoSequences.count > index)
                return EditorGUI.GetPropertyHeight(infoSequences.serializedProperty.GetArrayElementAtIndex(index)) + 2;
            else
                return 0;

            // Get the height of the InfoSequence at 'index' in list.
            // Get the height of the list of InfoScreen's in the InfoSequence
            // Get the height of each InfoScreen in the InfoSequence

        }

        private void DrawElementBackgroundCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            Rect drawRect = new Rect(rect.x, rect.y, rect.width, ElementHeightCallback(index));
            if (isActive && isFocused)
            {
                EditorGUI.DrawRect(drawRect, AugmeaEditorUtils.activeFocusedElementBackground);
            }
            else if (isActive)
            {
                EditorGUI.DrawRect(drawRect, AugmeaEditorUtils.activeElementBackground);
            }
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            //rect.y += 2;
            SerializedProperty element = infoSequences.serializedProperty.GetArrayElementAtIndex(index);
            

            Rect drawRect = new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight);

            EditorGUI.PropertyField(drawRect, element, new GUIContent(" -- Test Active"));
            drawRect.y += drawRect.height;
        }
        

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            serializedObject.Update();

            InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(closeSequenceTrigger);
            InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(skipScreenTrigger);
            InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(skipSequenceTrigger);
            InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(restartScreenTrigger);
            InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(restartSequenceTrigger);
            InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(startInfoDisplayTrigger);

            infoSequences.DoLayoutList();
            serializedObject.ApplyModifiedProperties();

            if (EditorGUI.EndChangeCheck())
            {
                //Debug.Log("Change");
                var infoDisplayManager = target as InfoDisplayManager;
                // infoDisplayManager.
            }
        }
    }
}