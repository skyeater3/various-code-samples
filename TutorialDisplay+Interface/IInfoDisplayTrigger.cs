using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoDisplayTriggerArgs : InfoDisplayEventArgs
{
    public InfoDisplayTriggerArgs(bool userInitiated)
        :base(userInitiated)
    {
    }
}

public interface IInfoDisplayTrigger
{
    event EventHandler<InfoDisplayTriggerArgs> Triggered;
}
