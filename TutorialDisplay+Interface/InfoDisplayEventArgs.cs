using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all InfoDisplay Event Arguments that are needed
/// </summary>
public class InfoDisplayEventArgs : EventArgs
{
    /// <summary>
    /// Time at which the event was invoked.
    /// </summary>
    public float timeStamp;
    /// <summary>
    /// Was this event initiated by user interaction, or an automated process?
    /// </summary>
    public bool userInitiated;

    protected InfoDisplayEventArgs(bool userInitiated)
    {
        timeStamp = Time.unscaledTime;
        this.userInitiated = userInitiated;
    }
}
