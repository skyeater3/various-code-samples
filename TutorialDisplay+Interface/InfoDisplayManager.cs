using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class InfoDisplayManager : MonoBehaviour 
{
	/// <summary>
	/// The trigger that will cause the currently active InfoScreen to be skipped. 
	/// </summary>
	[SerializeField]
	public GameObject skipScreenTrigger;

	public IInfoDisplayTrigger SkipScreenTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (skipScreenTrigger != null)
			{
				var matchType = skipScreenTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}

			return null;
		}
	}
	/// <summary>
	/// The trigger that will cause the entire sequence of InfoScreen's to be skipped.
	/// </summary>
	[SerializeField]
	public GameObject skipSequenceTrigger;

	public IInfoDisplayTrigger SkipSequenceTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (skipSequenceTrigger != null)
			{
				var matchType = skipSequenceTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}
	/// <summary>
	/// The trigger that will cause the currently active InfoScreen to restart.
	/// </summary>
	[SerializeField]
	public GameObject restartScreenTrigger;

	public IInfoDisplayTrigger RestartScreenTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (restartScreenTrigger != null)
			{
				var matchType = restartScreenTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}
	/// <summary>
	/// The trigger that will cause the entire sequence of InfoScreen's to restart.
	/// </summary>
	[SerializeField]
	public GameObject restartSequenceTrigger;

	public IInfoDisplayTrigger RestartSequenceTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (restartSequenceTrigger != null)
			{
				var matchType = restartSequenceTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}
	/// <summary>
	/// The trigger that will close the sequence (and the current screen).
	/// </summary>
	[SerializeField]
	public GameObject closeSequenceTrigger;

	public IInfoDisplayTrigger CloseSequenceTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (closeSequenceTrigger != null)
			{
				var matchType = closeSequenceTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}
	/// <summary>
	/// A trigger that will start the InfoDisplayManager system, to play a sequence of screens.
	/// </summary>
	[SerializeField]
	public GameObject startInfoDisplayTrigger;

	public IInfoDisplayTrigger StartInfoDisplayTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (startInfoDisplayTrigger != null)
			{
				var matchType = startInfoDisplayTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}

	/// <summary>
	/// The list of all available InfoSequences available to run through
	/// </summary>
	[SerializeField]
	public List<InfoSequence> infoSequences;
	/// <summary>
	/// The currently active sequence, if any.
	/// </summary>
	InfoSequence activeSequence;
	
	/// <summary>
	/// Register trigger handlers, and initialise active sequence to null
	/// </summary>
	private void Start() 
	{
		activeSequence = null;
		if (infoSequences == null)
		{
			throw new UnassignedReferenceException("No assigned infoSequences.");
		}
		RegisterTriggerHandlers();
	}

	private void RegisterTriggerHandlers()
	{
		// These triggers are optional
		if (SkipScreenTrigger != null)
		{
			SkipScreenTrigger.Triggered += OnSkipScreenTriggered;
		}
		if (SkipSequenceTrigger != null)
		{
			SkipSequenceTrigger.Triggered += OnSkipSequenceTriggered;
		}
		if (RestartScreenTrigger != null)
		{
			RestartScreenTrigger.Triggered += OnRestartScreenTriggered;
		}
		if (RestartSequenceTrigger != null)
		{
			RestartSequenceTrigger.Triggered += OnRestartSequenceTriggered;
		}
		if (StartInfoDisplayTrigger != null)
		{
			StartInfoDisplayTrigger.Triggered += OnStartInfoDisplayTriggered;
		}
		// this one is not optional
		if (CloseSequenceTrigger != null)
		{
			CloseSequenceTrigger.Triggered += OnCloseSequenceTriggered;
		}
		else
		{
			throw new UnassignedReferenceException("CloseSequenceTrigger has not been assigned. There is no way to close the sequence.");
		}
	}
	
	private void UnregisterTriggerHandlers()
	{
		if (SkipScreenTrigger != null)
		{
			SkipScreenTrigger.Triggered -= OnSkipScreenTriggered;
		}
		if (SkipSequenceTrigger != null)
		{
			SkipSequenceTrigger.Triggered -= OnSkipSequenceTriggered;
		}
		if (RestartScreenTrigger != null)
		{
			RestartScreenTrigger.Triggered -= OnRestartScreenTriggered;
		}
		if (RestartSequenceTrigger != null)
		{
			RestartSequenceTrigger.Triggered -= OnRestartSequenceTriggered;
		}
		if (StartInfoDisplayTrigger != null)
		{
			StartInfoDisplayTrigger.Triggered -= OnStartInfoDisplayTriggered;
		}
		if (CloseSequenceTrigger != null)
		{
			CloseSequenceTrigger.Triggered -= OnCloseSequenceTriggered;
		}
	}

	private void RegisterSequenceHandlers()
	{
		if (activeSequence != null)
		{
			activeSequence.SequenceClosed += OnSequenceClosed;
			activeSequence.SequenceFinished += OnSequenceFinished;
			activeSequence.SequenceStarted += OnSequenceStarted;
		}
	}

	private void UnregisterSequenceHandlers()
	{
		if (activeSequence != null)
		{
			activeSequence.SequenceClosed -= OnSequenceClosed;
			activeSequence.SequenceFinished -= OnSequenceFinished;
			activeSequence.SequenceStarted -= OnSequenceStarted;
		}
	}

	/// <summary>
	/// Unregister trigger handlers in case the triggers outlive the display manager.
	/// </summary>
	private void OnDestroy()
	{
		UnregisterTriggerHandlers();
		UnregisterSequenceHandlers();       
	}

	/// <summary>
	/// When the skip screen trigger is fired...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnSkipScreenTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log($"Skip screen triggered {(e.userInitiated ? "userInitiated" : "automated")}");
	}

	/// <summary>
	/// When the skip sequence trigger is fired...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnSkipSequenceTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log($"Skip sequence triggered {(e.userInitiated ? "userInitiated" : "automated")}");
	}

	/// <summary>
	/// When the restart screen trigger is fired...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnRestartScreenTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log($"Restart screen triggered {(e.userInitiated ? "userInitiated" : "automated")}");
	}

	/// <summary>
	/// When the restart sequence trigger is fired...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnRestartSequenceTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log($"Restart sequence triggered {(e.userInitiated ? "userInitiated" : "automated")}");
	}

	/// <summary>
	/// When the close sequence trigger is fired...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnCloseSequenceTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log($"Close screen triggered {(e.userInitiated ? "userInitiated" : "automated")}");
	}

	/// <summary>
	/// When the start info display trigger is fired...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnStartInfoDisplayTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log($"Start info display triggered {(e.userInitiated ? "userInitiated" : "automated")}");
		if (activeSequence == null)
		{
			activeSequence = infoSequences.First();
			RegisterSequenceHandlers();
			activeSequence.StartSequence(true);
		}
		else
		{
			activeSequence.CloseSequence(true);
			UnregisterSequenceHandlers();
			activeSequence = null;
		}
	}

	private void Update() 
	{
		if (Input.GetKeyDown(KeyCode.F1))
		{
			OnStartInfoDisplayTriggered(this, new InfoDisplayTriggerArgs(true));
		}
	}

	/// <summary>
	/// When the sequence reports that it has closed...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnSequenceClosed(object sender, SequenceClosedArgs e)
	{
		Debug.Log($"Sequence has been closed {(e.userInitiated ? "userInitiated" : "automated")}");
	}

	/// <summary>
	/// When the sequence reports that it has finished...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnSequenceFinished(object sender, SequenceFinishedArgs e)
	{
		Debug.Log($"Sequence has finished {(e.userInitiated ? "userInitiated" : "automated")}");
        activeSequence.CloseSequence(e.userInitiated);
        UnregisterSequenceHandlers();
        activeSequence = null;
	}

	/// <summary>
	/// When the sequence reports that it has started...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnSequenceStarted(object sender, SequenceStartedArgs e)
	{
		Debug.Log($"Sequence has started {(e.userInitiated ? "userInitiated" : "automated")}");
	}
}
