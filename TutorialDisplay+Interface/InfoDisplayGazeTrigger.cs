using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;

public class InfoDisplayGazeTrigger : MonoBehaviour, IInfoDisplayTrigger, IGazeStartEvent, IGazeEndEvent
{
	public event EventHandler<InfoDisplayTriggerArgs> Triggered;

	public float gazeTimeToTrigger = 1.0f;
	float elapsedGazeTime = 0.0f;
	bool isGazing = false;

	public void OnGazeEnd()
	{
		isGazing = false;
	}

	public void OnGazeStart()
	{
		elapsedGazeTime = 0.0f;
		isGazing = true;
	}

	// Awake
	void Awake()
	{
		
	}

	// Use this for initialization
	void Start() 
	{
		
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (isGazing)
		{
			if (elapsedGazeTime < gazeTimeToTrigger)
				elapsedGazeTime += Time.deltaTime;
			else
			{
				Triggered?.Invoke(this, new InfoDisplayTriggerArgs(true));
				isGazing = false;
			}
		}
	}
}
