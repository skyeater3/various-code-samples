using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class InterfaceLookup 
{
    static GameObject[] AllGameObjs;

    internal static Dictionary<Type, Dictionary<GameObject, IList>> interfaceLookup;

    static InterfaceLookup()
    {
        interfaceLookup = new Dictionary<Type, Dictionary<GameObject, IList>>();
    }

    public class Indices
    {
        public int[] array;

        public Indices(int count)
        {
            // initialises to 0 by default
            array = new int[count];
        }
    }

    public static void FindAllResourceOfType<T>()
    {
        AllGameObjs = Resources.FindObjectsOfTypeAll<GameObject>();
        Type type = typeof(T);
        if (!interfaceLookup.ContainsKey(type))
        {
            interfaceLookup.Add(type, new Dictionary<GameObject, IList>());
        }

        foreach (var go in AllGameObjs)
        {
            var matchingComponents = go.GetComponents<Component>().OfType<T>();
            if (!interfaceLookup[type].ContainsKey(go))
            {                
                if (matchingComponents.Any())
                {
                    interfaceLookup[type].Add(go, matchingComponents.ToList());
                }
            }
            else
            {
                // this gameobject was added to the dictionary at a previous time. Check that it is still valid.
                if (matchingComponents.Any())
                {
                    interfaceLookup[type][go] = matchingComponents.ToList();
                }
                else
                {
                    interfaceLookup[type].Remove(go);
                }
            }
        }
    }

    public static void FindAllResourceIndexOfType<T>(out Indices indices, params SerializedProperty[] properties)
    {
        bool[] bSet = new bool[properties.Length];
        Type type = typeof(T);
        indices = new Indices(properties.Length);

        if (interfaceLookup.ContainsKey(type))
        {
            var lookupTypeDict = interfaceLookup[type];
            if (lookupTypeDict.Count > 0)
            {
                for (int i = 0; i < lookupTypeDict.Count; i++)
                {
                    for (int j = 0; j < bSet.Length; j++)
                    {
                        if (!bSet[j])
                        {
                            if (lookupTypeDict.Keys.ElementAt(i) == properties[j].objectReferenceValue)
                            {
                                indices.array[j] = i + 1;
                                bSet[j] = true;
                            }
                        }
                    }
                    
                }
            }
        }
    }

    public static GUIContent[] GetContentForResourceOfType<T>()
    {
        List<GUIContent> content = new List<GUIContent>();
        content.Add(new GUIContent("None"));
        //content.Add(new GUIContent(EditorGUIUtility.ObjectContent(null, null)));
        if (interfaceLookup.ContainsKey(typeof(T)))
        {
            if (interfaceLookup[typeof(T)].Count > 0)
            {
                foreach (var matchingTriggerGO in interfaceLookup[typeof(T)].Keys)
                {
                    //content.Add(new GUIContent(matchingTriggerGO.name));
                    content.Add(new GUIContent(EditorGUIUtility.ObjectContent(matchingTriggerGO, typeof(T))));
                }
            }
        }
        return content.ToArray();
    }

    public static GameObject GetGameObjectOfType<T>(int index)
    {
        if (index != 0)
        {
            if (interfaceLookup.ContainsKey(typeof(T)))
            {
                return interfaceLookup[typeof(T)].Keys.ElementAt(index - 1);
            }
        }

        return null;
    }
}
