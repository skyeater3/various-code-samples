using UnityEditor;

namespace Augmea
{
	[CustomEditor(typeof(InfoScreen))]
	public class InfoScreenEditor : Editor
	{
		SerializedProperty nextScreenTrigger;
		SerializedProperty prevScreenTrigger;
		SerializedProperty DisplayPrefab;

		private void OnEnable()
		{
			InterfaceLookup.FindAllResourceOfType<IInfoDisplayTrigger>();

			nextScreenTrigger = serializedObject.FindProperty("nextScreenTrigger");
			prevScreenTrigger = serializedObject.FindProperty("prevScreenTrigger");
			DisplayPrefab = serializedObject.FindProperty("DisplayPrefab");

		}

		public override void OnInspectorGUI()
		{
			InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(nextScreenTrigger);
			InterfaceLookupField.InterfaceLookupFieldLayout<IInfoDisplayTrigger>(prevScreenTrigger);

			EditorGUILayout.ObjectField(DisplayPrefab);

			serializedObject.ApplyModifiedProperties();
		}
	}
}