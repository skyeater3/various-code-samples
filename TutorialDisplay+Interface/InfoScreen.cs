using System;
using System.Linq;
using UnityEngine;

public class NextScreenArgs : InfoDisplayEventArgs
{
	public NextScreenArgs(bool userInitiated)
		: base(userInitiated)
	{
	}
}

public class PrevScreenArgs : InfoDisplayEventArgs
{
	public PrevScreenArgs(bool userInitiated)
		: base(userInitiated)
	{
	}
}

public class ScreenOpenedArgs : InfoDisplayEventArgs
{
	public int screenId;

	public ScreenOpenedArgs(int id, bool userInitiated)
		: base(userInitiated)
	{
		screenId = id;
	}
}

public class ScreenClosedArgs : InfoDisplayEventArgs
{
	public int screenId;

	public ScreenClosedArgs(int id, bool userInitiated)
		: base(userInitiated)
	{
		screenId = id;
	}
}

[Serializable]
public class InfoScreen : MonoBehaviour
{
	#region Events
	/// <summary>
	/// Event fired when the next screen is being requested.
	/// </summary>
	public event EventHandler<NextScreenArgs> NextScreen;
	/// <summary>
	/// Event fired when the previous screen is being requested.
	/// </summary>
	public event EventHandler<PrevScreenArgs> PrevScreen;
	/// <summary>
	/// Event fired when this InfoScreen is opened.
	/// This even will be sent as a result of normal operation when navigating between screens.
	/// Unlikely to be necessary. You probably want to just add code to Open (which fires this event).
	/// </summary>
	public event EventHandler<ScreenOpenedArgs> OpenedScreen;
	/// <summary>
	/// Event fired when this InfoScreen is closed.
	/// This event will be sent as a result of normal operation when navigating between screens and when the InfoSequence is manually closed by the user.
	/// Unlikely to be necessary. You probably want to just add code to Close (which fires this event).
	/// </summary>
	public event EventHandler<ScreenClosedArgs> ClosedScreen;
	#endregion

	#region Members
	/// <summary>
	/// The trigger that will cause the next screen in the sequence to be requested.
	/// </summary>
	[SerializeField]
	public GameObject nextScreenTrigger;

	/// <summary>
	/// The trigger that will cause the previous screen in the sequence to be requested.
	/// </summary>
	public IInfoDisplayTrigger NextScreenTrigger
	{
		get
		{
			// get all components in the nextScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (nextScreenTrigger != null)
			{
				var matchType = nextScreenTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}

	/// <summary>
	/// The trigger that will cause the previous screen in the sequence to be requested.
	/// </summary>
	[SerializeField]
	public GameObject prevScreenTrigger;

	/// <summary>
	/// The trigger that will cause the previous screen in the sequence to be requested.
	/// </summary>
	public IInfoDisplayTrigger PrevScreenTrigger
	{
		get
		{

			// get all components in the prevScreenTrigger gameObject, then filter them by type to match the desired Interface.
			if (prevScreenTrigger != null)
			{
				var matchType = prevScreenTrigger?.GetComponents(typeof(Component)).OfType<IInfoDisplayTrigger>();
				// if there are any matching components, return the first one.
				if (matchType.Any())
				{
					return matchType.First();
				}
			}
			return null;
		}
	}

	/// <summary>
	/// Index of active screen in sequence - set at open
	/// </summary>
	public int ScreenId { get; private set; }

	// Prefabs for display
	[SerializeField]
	public GameObject DisplayPrefab;
	private GameObject _display;
	#endregion

	/// <summary>
	/// Register handlers for triggers used by this screen
	/// </summary>
	private void RegisterTriggerHandlers()
	{
		if (NextScreenTrigger == null)
		{
			throw new UnassignedReferenceException("NextScreenTrigger has not been assigned, there is no way to move forward from this screen except by forcibly quitting the info display sequence.");
		}
		else
		{
			NextScreenTrigger.Triggered += OnNextScreenTriggered;
		}
		if (PrevScreenTrigger == null)
		{
			throw new UnassignedReferenceException("PrevScreenTrigger has not been assigned, there is no way to move back from this screen except by forcibly restarting the info display sequence.");
		}
		else
		{
			PrevScreenTrigger.Triggered += OnPrevScreenTriggered;
		}
	}

	/// <summary>
	/// Unregister handlers for triggers used by this screen.
	/// </summary>
	private void UnregisterTriggerHandlers()
	{
		if (NextScreenTrigger != null)
		{
			NextScreenTrigger.Triggered -= OnNextScreenTriggered;
		}
		if (PrevScreenTrigger != null)
		{
			PrevScreenTrigger.Triggered -= OnPrevScreenTriggered;
		}
	}

	/// <summary>
	/// When this InfoScreen's NextScreenTrigger is activated, send the NextScreen event.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnNextScreenTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log("Next Screen triggered");
		NextScreen?.Invoke(this, new NextScreenArgs(e.userInitiated));
	}

	/// <summary>
	/// When this InfoScreen's PrevScreenTrigger is activated, send the PrevScreen event.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	private void OnPrevScreenTriggered(object sender, InfoDisplayTriggerArgs e)
	{
		Debug.Log("Prev Screen triggered");
		PrevScreen?.Invoke(this, new PrevScreenArgs(e.userInitiated));
	}

	/// <summary>
	/// When this InfoScreen is opened for the first time, send the OpenScreen event.
	/// 
	/// </summary>
	/// <param name="id">Index of the screen in the sequence list.</param>
	public void Open(int id = 0, bool userInitiated = false)
	{
		Debug.Log($"Open Screen {id}");
		ScreenId = id;
		RegisterTriggerHandlers();

		if (DisplayPrefab != null)
		{
			if (_display != null)   // if necessary destroy the old display instance.
			{
				Destroy(_display);
				_display = null;
			}

			if (DisplayPrefab.scene == null)
				_display = Instantiate(DisplayPrefab);
			else
				throw new NotSupportedException("DisplayPrefab assigned to InfoScreen must not be an object existing in the scene.");

		}

		OpenedScreen?.Invoke(this, new ScreenOpenedArgs(ScreenId, userInitiated));
	}

	/// <summary>
	/// When this InfoScreen is closed, send the ClosedScreen event.
	/// </summary>
	/// <param name="closedByUser">Was this screen closed by the user or by some automatic method?</param>
	public void Close(bool userInitiated = false)
	{
		Debug.Log($"Open Screen {ScreenId}");
		UnregisterTriggerHandlers();

		if (_display != null)
		{
			Destroy(_display);
			_display = null;
		}

		ClosedScreen?.Invoke(this, new ScreenClosedArgs(ScreenId, userInitiated));
	}

	/// <summary>
	/// RegisterTriggerHandlers and do anything else that needs to be done on Start
	/// </summary>
	void Start()
	{
		RegisterTriggerHandlers();

		if (DisplayPrefab == null)
		{
			throw new UnassignedReferenceException("DisplayPrefab is null. Without assigning a DisplayPrefab to this InfoScreen, nothing will be displayed when the InfoScreen is Opened.");
		}
		_display = null;
	}

	/// <summary>
	/// On Destroy, remove calls to InfoScreen functions from Trigger, in case trigger is not also destroyed.
	/// </summary>
	void OnDestroy()
	{
		UnregisterTriggerHandlers();

		if (_display != null)
		{
			Destroy(_display);
			_display = null;
		}
	}
}
