using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SequenceStartedArgs : InfoDisplayEventArgs
{
	public SequenceStartedArgs(bool userInitiated)
		:base(userInitiated)
	{
	}
}

public class SequenceFinishedArgs : InfoDisplayEventArgs
{
	public SequenceFinishedArgs(bool userInitiated)
		:base(userInitiated)
	{
	}
}

public class SequenceClosedArgs : InfoDisplayEventArgs
{
	public SequenceClosedArgs(bool userInitiated)
		:base(userInitiated)
	{
	}
}

[Serializable]
public class InfoSequence : MonoBehaviour
{
	/// <summary>
	/// Event fired when the sequence starts, or restarts.
	/// </summary>
	public event EventHandler<SequenceStartedArgs> SequenceStarted;
	/// <summary>
	/// Event fired when the sequence finishes.
	/// </summary>
	public event EventHandler<SequenceFinishedArgs> SequenceFinished;
	/// <summary>
	/// Event fired when the sequence is closed.
	/// </summary>
	public event EventHandler<SequenceClosedArgs> SequenceClosed;

	/// <summary>
	/// The List containing all InfoScreens in this sequence.
	/// </summary>
	[SerializeField]
	public List<InfoScreen> infoScreens;
	/// <summary>
	/// The currently active InfoScreen, if any
	/// </summary>
	private InfoScreen activeScreen;
	
	/// <summary>
	/// When a NextScreen event is triggered, move to the next screen in the sequence.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public void OnNextScreenRequest(object sender, NextScreenArgs e)
	{
		Debug.Log($"Moving to next screen {(activeScreen != null ? activeScreen.ScreenId + 1: -1)}.");
		if (activeScreen == null)
		{
			SetActiveScreen(infoScreens.First(), e.userInitiated);
		}
		else
		{
			if (activeScreen == infoScreens.Last())
			{
				FinishSequence(e.userInitiated);
				return;
			}
			else
			{
				SetActiveScreen(infoScreens[activeScreen.ScreenId + 1], e.userInitiated);
			}
		}
	}

	/// <summary>
	/// When a PrevScreen event is triggered, move to the previous screen in the sequence.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public void OnPrevScreenRequest(object sender, PrevScreenArgs e)
	{
		Debug.Log($"Moving to prev screen {(activeScreen != null ? activeScreen.ScreenId : -1)}.");
		if (activeScreen == null)
		{
			SetActiveScreen(infoScreens.First(), e.userInitiated);
		}
		else
		{
			if (activeScreen == infoScreens.First())
			{
				StartSequence(e.userInitiated);
				return;
			}
			else
			{
				SetActiveScreen(infoScreens[activeScreen.ScreenId - 1], e.userInitiated);
			}
		}
	}

	/// <summary>
	/// When a ScreenOpened event is triggered...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public void OnScreenOpened(object sender, ScreenOpenedArgs e)
	{
		Debug.Log($"Screen {e.screenId} opened.");
	}

	/// <summary>
	/// When a ScreenClosed event is triggered...
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public void OnScreenClosed(object sender, ScreenClosedArgs e)
	{
		Debug.Log($"Screen {e.screenId} closed. {(e.userInitiated ? "Closed by user request" : "Not closed by user request")}.");

	}

	/// <summary>
	/// Set the Active Screen in the sequence
	/// </summary>
	/// <param name="screen">The screen to be set as active (or null)</param>
	/// <param name="userInitiated">Was this action initiated by a user interaction, or an automated sequence.</param>
	private void SetActiveScreen(InfoScreen screen, bool userInitiated)
	{        
		if (activeScreen)
		{
			// close and remove handlers from activeScreen.
			UnregisterScreenHandlers();
			activeScreen.Close(userInitiated);            
		}

		if (screen)
		{
			activeScreen = screen;
			RegisterScreenHandlers();
			// then open the activeScreen
			activeScreen.Open(infoScreens.IndexOf(screen));
		}
		else
		{
			activeScreen = null;
		}
	}
		
	// Use this for initialization
	void Start()
	{
		activeScreen = null;
		if (infoScreens == null)
		{
			throw new UnassignedReferenceException("This sequence has no assigned InfoScreens.");
		}
	}

	private void OnDestroy()
	{
		UnregisterScreenHandlers();
	}

	private void RegisterScreenHandlers()
	{
		if (activeScreen != null)
		{
			activeScreen.OpenedScreen += OnScreenOpened;
			activeScreen.ClosedScreen += OnScreenClosed;
			activeScreen.NextScreen += OnNextScreenRequest;
			activeScreen.PrevScreen += OnPrevScreenRequest;
		}
	}

	private void UnregisterScreenHandlers()
	{
		if (activeScreen != null)
		{
			activeScreen.OpenedScreen -= OnScreenOpened;
			activeScreen.ClosedScreen -= OnScreenClosed;
			activeScreen.NextScreen -= OnNextScreenRequest;
			activeScreen.PrevScreen -= OnPrevScreenRequest;
		}
	}

	/// <summary>
	/// When StartSequence has been called, set the first screen in the sequence to active, and trigger the sequenceStarted event.
	/// </summary>
	/// <param name="userInitiated"></param>
	public void StartSequence(bool userInitiated = false)
	{
		Debug.Log($"Starting Sequence.");
		SetActiveScreen(infoScreens.First(), userInitiated);
		SequenceStarted?.Invoke(this, new SequenceStartedArgs(userInitiated));
	}

	/// <summary>
	/// When CloseSequence is called, set the activeScreen to null, and trigger the SequenceClosed event.
	/// </summary>
	/// <param name="userInitiated"></param>
	public void CloseSequence(bool userInitiated = false)
	{
		Debug.Log($"Closing Sequence.");
		SetActiveScreen(null, userInitiated);
		SequenceClosed?.Invoke(this, new SequenceClosedArgs(userInitiated));
	}

	/// <summary>
	/// When FinishSequence is called, set the active screen to null, and trigger the SequenceFinished event.
	/// </summary>
	/// <param name="userInitiated"></param>
	private void FinishSequence(bool userInitiated = false)
	{
		Debug.Log($"Finishing Sequence.");
		SetActiveScreen(null, userInitiated);
		SequenceFinished?.Invoke(this, new SequenceFinishedArgs(userInitiated));
	}

	// Update is called once per frame
	//void Update()
	//{

	//}
}

