using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Linq;

namespace Augmea
{
    [CustomEditor(typeof(InfoSequence))]
    public class InfoSequenceEditor : Editor
    {
        private ReorderableList infoScreens;

        private void OnEnable()
        {
            serializedObject.Update();

            infoScreens = new ReorderableList(serializedObject, serializedObject.FindProperty("infoScreens"), true, true, true, true);
            infoScreens.drawElementCallback = DrawElementCallback;
            infoScreens.elementHeightCallback = ElementHeightCallback;
            infoScreens.drawHeaderCallback = DrawHeaderCallback;
            infoScreens.drawElementBackgroundCallback = DrawElementBackgroundCallback;
        }

        private void DrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, "Info Screens");
        }

        private float ElementHeightCallback(int index)
        {
            Repaint();
            return (EditorGUIUtility.singleLineHeight) * 2;
        }

        private void DrawElementBackgroundCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            Rect drawRect = new Rect(rect.x, rect.y, rect.width, ElementHeightCallback(index));
            if (isActive && isFocused)
            {
                EditorGUI.DrawRect(drawRect, AugmeaEditorUtils.activeFocusedElementBackground);
            }
            else if (isActive)
            {
                EditorGUI.DrawRect(drawRect, AugmeaEditorUtils.activeElementBackground);
            }
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            rect.y += 2;
            SerializedProperty element = infoScreens.serializedProperty.GetArrayElementAtIndex(index);

            Rect drawRect = new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight);

            EditorGUI.PropertyField(drawRect, element, new GUIContent($"{index}"));
            drawRect.y += drawRect.height;
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            serializedObject.Update();
            infoScreens.DoLayoutList();
            serializedObject.ApplyModifiedProperties();

            if (EditorGUI.EndChangeCheck())
            {
                //Debug.Log("Change");
                var infoSequence = target as InfoSequence;
            }
        }
    }
}