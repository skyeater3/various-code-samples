using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public static class InterfaceLookupField
{
    public static int InterfaceLookupFieldLayout<T>(SerializedProperty property)
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.PrefixLabel(ObjectNames.NicifyVariableName(property.name));
        GUIContent gUIContent = EditorGUIUtility.ObjectContent(property.objectReferenceValue, typeof(T));
        GUILayout.Box(gUIContent, UnityEditor.EditorStyles.objectField, GUILayout.MaxHeight(EditorGUIUtility.singleLineHeight));

        var lastRect = GUILayoutUtility.GetLastRect();
        lastRect.xMin = lastRect.xMax - 16;

        int propWindowId = 0;
        if (GUI.Button(lastRect, GUIContent.none, GUIStyle.none))
        {
            propWindowId = InterfaceLookupWindow.ShowWindow<T>(property);
        }

        //if (Event.current.type == InterfaceLookupWindow.InterfaceLookupWindowUpdatedSelectedObject.type 
        //    && Event.current.commandName == InterfaceLookupWindow.InterfaceLookupWindowUpdatedSelectedObject.commandName
        //    && Event.current.displayIndex == propWindowId)
        //{
        //    property.objectReferenceValue = 
        //}

        EditorGUILayout.EndHorizontal();

        return 0;
    }
}

public static class InterfaceLookupWindow
{
    internal static EditorWindow windowToReceiveEvents;

    static InterfaceLookupWindow()
    {
        SelectAndClose = new Event();
        SelectAndClose.type = EventType.ExecuteCommand;
        SelectAndClose.commandName = "SelectAndClose";

        InterfaceLookupWindowUpdatedSelectedObject = new Event();
        InterfaceLookupWindowUpdatedSelectedObject.type = EventType.ExecuteCommand;
        InterfaceLookupWindowUpdatedSelectedObject.commandName = "InterfaceLookupWindowUpdatedSelectedObject";

        InterfaceLookupWindowClosed = new Event();
        InterfaceLookupWindowClosed.type = EventType.ExecuteCommand;
        InterfaceLookupWindowClosed.commandName = "InterfaceLookupWindowClosed";
    }

    public static int ShowWindow<T>(SerializedProperty p)
    {
        windowToReceiveEvents = InspectorWindow.focusedWindow;

        InterfaceLookupWindowWrapper window = EditorWindow.GetWindow<InterfaceLookupWindowWrapper>("Interface Lookup Window");
        window.Type = typeof(T);

        window.property = p;

        return window.GetInstanceID();
    }

    public static Event SelectAndClose;
    public static Event InterfaceLookupWindowUpdatedSelectedObject;
    public static Event InterfaceLookupWindowClosed;
}

public interface ILookupWindow
{
    void Awake(InterfaceLookupWindowWrapper thisWindow);
    void Update(InterfaceLookupWindowWrapper thisWindow);
    void OnGUI(InterfaceLookupWindowWrapper thisWindow);
    void OnDestroy(InterfaceLookupWindowWrapper thisWindow);
    void OnLostFocus(InterfaceLookupWindowWrapper thisWindow);
    void OnSelectionChange(InterfaceLookupWindowWrapper thisWindow);
    void OnEnable(InterfaceLookupWindowWrapper thisWindow);
    void OnDisable(InterfaceLookupWindowWrapper thisWindow);
    void OnFocus(InterfaceLookupWindowWrapper thisWindow);
    void OnHeirarchyChange(InterfaceLookupWindowWrapper thisWindow);
    void OnInspectorChange(InterfaceLookupWindowWrapper thisWindow);
    void OnProjectChange(InterfaceLookupWindowWrapper thisWindow);
}


public class InterfaceLookupWindowWrapper : EditorWindow
{
    Type _type = null;
    object _impl = null;

    internal SerializedProperty property;

    public System.Type Type
    {
        get { return _type.GetGenericArguments()[0]; }
        set
        {
            // These two lines are how you get your specific implementation
            _type = typeof(InterfaceLookupWindow<>).MakeGenericType(value);
            _impl = System.Activator.CreateInstance(_type);
            OnEnable();
        }
    }

    void OnGUI()
    {
        (_impl as ILookupWindow)?.OnGUI(this);
    }
    //void Awake()
    //{
    //    (_impl as ILookupWindow)?.Awake(this);
    //}
    void Update()
    {
        (_impl as ILookupWindow)?.Update(this);
    }
    void OnDestroy()
    {
        (_impl as ILookupWindow)?.OnDestroy(this);
    }
    void OnLostFocus()
    {
        (_impl as ILookupWindow)?.OnLostFocus(this);
    }
    void OnSelectionChange()
    {
        (_impl as ILookupWindow)?.OnSelectionChange(this);
    }
    void OnEnable()
    {
        (_impl as ILookupWindow)?.OnEnable(this);
    }
    void OnDisable()
    {
        (_impl as ILookupWindow)?.OnDisable(this);
    }
    void OnFocus()
    {
        (_impl as ILookupWindow)?.OnFocus(this);
    }
    void OnHeirarchyChange()
    {
        (_impl as ILookupWindow)?.OnHeirarchyChange(this);
    }
    void OnInspectorChange()
    {
        (_impl as ILookupWindow)?.OnInspectorChange(this);
    }
    void OnProjectChange()
    {
        (_impl as ILookupWindow)?.OnProjectChange(this);
    }
}

public class InterfaceLookupWindow<T> : ILookupWindow 
{	
	Vector2 scrollPos;
    bool startedClick = false;
    int clickIndex = 0;
    int selectedIndex = 0;
    double clickDelay = 1.0f;
    double initialClick = 0.0f;
    
    public void OnDestroy(InterfaceLookupWindowWrapper thisWindow)
	{
        InterfaceLookupWindow.windowToReceiveEvents.SendEvent(new Event(InterfaceLookupWindow.InterfaceLookupWindowClosed));
	}

    public void OnLostFocus(InterfaceLookupWindowWrapper thisWindow)
	{
		//Debug.Log("Lost Focus");
        thisWindow.Close();
	}

    public void Update(InterfaceLookupWindowWrapper thisWindow)
	{
        if (startedClick)
        {
            clickDelay = EditorApplication.timeSinceStartup - initialClick;
            if (clickDelay > 1.0f)
                startedClick = false;
        }
    }

    public void OnEnable(InterfaceLookupWindowWrapper thisWindow)
	{
		
	}

    public void OnSelectionChange(InterfaceLookupWindowWrapper thisWindow)
	{
		//Debug.Log("SelectionChange");
	}

	public void OnGUI(InterfaceLookupWindowWrapper thisWindow)
	{
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginScrollView(scrollPos);

        InterfaceLookup.FindAllResourceOfType<T>();
        //InterfaceLookup.Indices indices;
        //InterfaceLookup.FindAllResourceIndexOfType<T>(out indices, )
        var content = InterfaceLookup.GetContentForResourceOfType<T>();

        EditorGUI.BeginChangeCheck();
        for (int i = 0; i < InterfaceLookup.interfaceLookup[typeof(T)].Count + 1; i++)
        {
            var iterateLookup = i > 0 ? InterfaceLookup.interfaceLookup[typeof(T)].Keys.ElementAt(i - 1) : null;
            if (GUILayout.Button(content[i], iterateLookup == thisWindow.property.objectReferenceValue ? EditorStyles.boldLabel : EditorStyles.label, GUILayout.MaxHeight(EditorGUIUtility.singleLineHeight)))
            {
                if (startedClick && i == clickIndex)
                {
                    if (clickDelay < 0.25f)
                    {
                        //if (Event.current.type == InterfaceLookupWindow.SelectAndClose.type && Event.current.commandName == InterfaceLookupWindow.SelectAndClose.commandName)
                        {                            
                            thisWindow.Close();
                            return;
                        }
                    }
                    else
                    {
                        startedClick = false;
                    }
                }
                else if (i > 0)
                {
                    thisWindow.property.objectReferenceValue = iterateLookup;
                    //InterfaceLookupWindow.windowToReceiveEvents.SendEvent(new Event(InterfaceLookupWindow.InterfaceLookupWindowUpdatedSelectedObject));

                    EditorGUIUtility.PingObject(iterateLookup);
                    startedClick = true;
                    clickIndex = i;
                    initialClick = EditorApplication.timeSinceStartup;
                }
                else if (i == 0)
                {
                    thisWindow.property.objectReferenceValue = null;
                    startedClick = true;
                    clickIndex = i;
                    initialClick = EditorApplication.timeSinceStartup;
                }
            }
        }

        if (EditorGUI.EndChangeCheck())
            thisWindow.property.serializedObject.ApplyModifiedProperties();
        
        EditorGUILayout.EndScrollView();
        GUILayout.FlexibleSpace();

        var orv = thisWindow.property.objectReferenceValue;
        if (orv != null)
        {
            var label = orv.name + "\n" + orv.GetType().Name + "\n";
            var assetPath = AssetDatabase.GetAssetPath(orv);
            if (string.IsNullOrEmpty(assetPath))
            {
                var t = (orv as GameObject).transform;
                assetPath = t.name;
                while (t.parent != null)
                {
                    t = t.parent;
                    assetPath = string.Concat(t.name + "/", assetPath);
                }
                assetPath = string.Concat($"In scene: {t.gameObject.scene.name}/", assetPath);
            }
            GUILayout.Label(label + assetPath);
        }
        EditorGUILayout.EndVertical();
	}

    public void Awake(InterfaceLookupWindowWrapper thisWindow)
    {
        
    }

    public void OnDisable(InterfaceLookupWindowWrapper thisWindow)
    {
        
    }

    public void OnFocus(InterfaceLookupWindowWrapper thisWindow)
    {
        
    }

    public void OnHeirarchyChange(InterfaceLookupWindowWrapper thisWindow)
    {
        
    }

    public void OnInspectorChange(InterfaceLookupWindowWrapper thisWindow)
    {
        
    }

    public void OnProjectChange(InterfaceLookupWindowWrapper thisWindow)
    {
        
    }
}

