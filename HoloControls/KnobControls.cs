using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;

public class KnobControls : PhysicalControl, IManipulationHandler
{
    public float minValue = 0.0f;
    public float maxValue = 10.0f;
    public float initialValue = 0.0f;

    public float maxAngle = 150.0f;
    public float minAngle = -150.0f;
    private float AngleRange { get { return maxAngle - minAngle; } }

    public float m_value;
    public float Value
    {
        get { return m_value; }
        private set
        {
            m_value = Mathf.Clamp(value, minValue, maxValue);
        }
    }

    public bool snapToIntervals = false;
    private int currentInterval;
    public int numberOfIntervals = 10;
    private float RangeBetweenIntervals { get { return ((maxValue - minValue) / numberOfIntervals); } }   // degrees, with central interval being at 0;
    
    public float speed = 10.0f;
    private Vector2 lastCumulativeDelta = new Vector2(0, 0);

    public GameObject Dial;

    // Use this for initialization
    void Awake()
    {
        Value = initialValue;
        currentInterval = Mathf.FloorToInt(Value / RangeBetweenIntervals);
        SetDialLocalRotationFromValue();
		OnControlChanged();

	}

    private void SetDialLocalRotationFromValue()
    {
        if (Dial)
        {
            // Convert from Value to Rotation
            // On a 12 point system, with only 11 points (0-10)
            // Where 5 is at rotation 0;
            // 0 at -150 and 10 at 150;

            float rotation = ((maxValue - minValue) / 100.0f) * Value * AngleRange - (AngleRange / 2.0f);            
            Dial.transform.localRotation = Quaternion.Euler(0, -rotation, 0);
        }
    }

    public void OnManipulationCanceled(ManipulationEventData eventData)
    {
        InputManager.Instance.PopModalInputHandler();
    }

    public void OnManipulationCompleted(ManipulationEventData eventData)
    {
        InputManager.Instance.PopModalInputHandler();
    }

    public void OnManipulationStarted(ManipulationEventData eventData)
    {
        InputManager.Instance.PushModalInputHandler(this.gameObject);
        lastCumulativeDelta = new Vector2(0.0f, 0.0f);
    }

    public void OnManipulationUpdated(ManipulationEventData eventData)
    {
        var worldDelta = eventData.CumulativeDelta;
        var cameraTransform = Camera.main.transform;
        var localDelta = cameraTransform.InverseTransformVector(worldDelta);

        var delta = (Vector2)localDelta;
        var diff = (delta - lastCumulativeDelta) * speed;

		if (!snapToIntervals)
		{
			// TODO: try allow rotational control , air-wheel finger movements rather than side-to-side
			Value += diff.x;
			lastCumulativeDelta = delta;
			OnControlChanged();
		}
		else
		{
			if (diff.x > RangeBetweenIntervals)
			{
				currentInterval++;
				Value = currentInterval * RangeBetweenIntervals;
				lastCumulativeDelta = eventData.CumulativeDelta;
				OnControlChanged();
			}
			else if (diff.x < -RangeBetweenIntervals)
			{
				currentInterval--;
				Value = currentInterval * RangeBetweenIntervals;
				lastCumulativeDelta = eventData.CumulativeDelta;
				OnControlChanged();
			}
		}

        SetDialLocalRotationFromValue();
    }

	public override float GetControlValue()
	{
		return Value;
	}

	public override float GetValueMax()
	{
		return maxValue;
	}

	public override float GetValueMin()
	{
		return minValue;
	}
}
