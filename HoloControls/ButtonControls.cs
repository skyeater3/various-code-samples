using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;

public class ButtonControls : PhysicalControl, IInputClickHandler
{
	public bool ToggleState { get; private set; }
	public char ButtonValue;
	public AnimationClip clickAnimation;
	public GameObject animatedObject;
	public GameObject labelObject;
	public Material On_Material;
	public Material Off_Material;
	public bool bToggle = false;

	private string PressAnimationName { get { return "Press"; } }

	public void OnInputClicked(InputClickedEventData eventData)
	{
		if (animatedObject)
		{
			var existingAnim = animatedObject.GetComponent<Animation>();
			if (existingAnim)
			{
				if (bToggle && !ToggleState)
				{
					existingAnim[PressAnimationName].speed = 1;
					existingAnim[PressAnimationName].time = 0;
				}
				else
				{
					existingAnim[PressAnimationName].speed = -1;
					existingAnim[PressAnimationName].time = existingAnim[PressAnimationName].length;
				}

				existingAnim.Play(PressAnimationName);
			}
		}

		ToggleState = !ToggleState;

		if (labelObject)
		{
			labelObject.GetComponent<MeshRenderer>().material.CopyPropertiesFromMaterial(ToggleState ? On_Material : Off_Material);
			var localPos = labelObject.transform.localPosition;
			localPos = new Vector3(localPos.x, localPos.y, localPos.z + (ToggleState ? 0.005f : -.005f));
			labelObject.transform.localPosition = localPos;
		}

		OnControlChanged();

		if (!bToggle)
		{
			ToggleState = !ToggleState;
		}
	}

	// Use this for initialization
	void Awake ()
	{
		if (animatedObject && clickAnimation)
		{
			var existingAnim = animatedObject.GetComponent<Animation>();
			if (!existingAnim)
			{
				existingAnim = animatedObject.AddComponent<Animation>();
			}

			existingAnim.AddClip(clickAnimation, PressAnimationName);
		}
	}

	public override float GetControlValue()
	{
		return bToggle ? (ToggleState ? 1.0f : 0.0f) : ButtonValue;
	}

	public override float GetValueMax()
	{
		return 1.0f;
	}

	public override float GetValueMin()
	{
		return 0.0f;
	}
}
