using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IPhysicalControl
{
	float GetControlValue();
	void OnControlChanged();
}

public abstract class PhysicalControl : MonoBehaviour, IPhysicalControl
{
	[Serializable]
	public class ControlEvent : UnityEvent<float> { };

	public ControlEvent onControlChangedEvent;

	public abstract float GetControlValue();
	public abstract float GetValueMax();
	public abstract float GetValueMin();

	public void OnControlChanged()
	{
		onControlChangedEvent.Invoke(GetControlValue());
	}
}
