using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;

public class SliderControls : PhysicalControl, /*IInputClickHandler,*/ /*IHoldHandler,*/ IManipulationHandler
{
    public float minValue = 0.0f;
    public float maxValue = 1.0f;
    public float startValue = 0.0f;

    public bool displayIntervals = true;    
    public int numberOfIntervals = 10;
    public bool snapToIntervals = false;
    private int currentInterval;
    public float speed = 3.5f;
    private float RangeBetweenIntervals { get { return (maxValue - minValue) / numberOfIntervals; } }

    public GameObject SliderTrack;
    public GameObject SliderHandle;
    
    private float m_value;    
    public float Value
    {
        get
        {
            return m_value;
        }
        private set
        {
            m_value = Mathf.Clamp(value, minValue, maxValue);
        }
    }
    private float lastCumulativeDeltaX = 0.0f;

    private void Awake()
    {
        Value = startValue;
        currentInterval = Mathf.FloorToInt(Value / ((maxValue - minValue) / numberOfIntervals));

        SetSliderHandleLocalPositionFromValue();
		OnControlChanged();
	}

	public void OnManipulationCanceled(ManipulationEventData eventData)
    {
        InputManager.Instance.PopModalInputHandler();
    }

    public void OnManipulationCompleted(ManipulationEventData eventData)
    {
        InputManager.Instance.PopModalInputHandler();
    }

    public void OnManipulationStarted(ManipulationEventData eventData)
    {
        InputManager.Instance.PushModalInputHandler(this.gameObject);
        lastCumulativeDeltaX = 0.0f;
    }

    public void OnManipulationUpdated(ManipulationEventData eventData)
    {
        var worldDelta = eventData.CumulativeDelta;
        var cameraTransform = Camera.main.transform;
        var localDelta = cameraTransform.InverseTransformVector(worldDelta);

        var diff = (localDelta.x - lastCumulativeDeltaX) * speed;
        //bool snapped = false;
        if (snapToIntervals)
        {
            if (diff > RangeBetweenIntervals)
            {                
                currentInterval++;
                Value = currentInterval * RangeBetweenIntervals;
                lastCumulativeDeltaX = localDelta.x;
				OnControlChanged();
			}
			else if (diff < -RangeBetweenIntervals)
            {
                currentInterval--;
                Value = currentInterval * RangeBetweenIntervals;
                lastCumulativeDeltaX = localDelta.x;
				OnControlChanged();
			}
		}

        //if (!snapped)
        else
        {
            Value += diff;
            lastCumulativeDeltaX = localDelta.x;
			OnControlChanged();
		}

		SetSliderHandleLocalPositionFromValue();
    }

    private void SetSliderHandleLocalPositionFromValue()
    {
        if (SliderHandle && SliderTrack)
        {
            var trackLocalPos = SliderTrack.transform.localPosition;
            var trackLocalScale = SliderTrack.transform.localScale;

            SliderHandle.transform.localPosition = new Vector3((trackLocalPos.x - trackLocalScale.x / 2.0f) + (((maxValue - minValue) / 100.0f) * Value) * trackLocalScale.x * 100.0f,
                            SliderHandle.transform.localPosition.y,
                            SliderHandle.transform.localPosition.z);
        }
    }

	public override float GetControlValue()
	{
		return Value;
	}

	public override float GetValueMax()
	{
		return maxValue;
	}

	public override float GetValueMin()
	{
		return minValue;
	}
}
