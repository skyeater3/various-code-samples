using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCvSharp;
using System.Runtime.InteropServices;
using Meta;
using System;

public class AugKalmanFilter : MonoBehaviour
{
	/// <summary>
	/// To receive Tracking ready events
	/// </summary>
	private ISlamEventProvider slamEventProvider;

	/// <summary>
	/// OpenCv implementation of the kernelFilter
	/// </summary>
	KalmanFilter kalmanFilter;

	/// <summary>
	/// The metaCameraRig gameObject used to get the position of the camera in Unity worldspace (centred, not left / right eyes)
	/// </summary>
	[SerializeField]
	public GameObject cameraObject;

	/// <summary>
	/// Origin at time when tracking ready event received
	/// </summary>
	public Vector3 origin;

	//[SerializeField]
	//public GameObject gaussianSmoothing;

	/// <summary>
	/// Kalman Filter estimate of the currentPosition
	/// </summary>
	public Vector3 kfEstimate
	{
		get
		{
			if (IsRunning)
				return new Vector3(kalmanFilter.StatePost.At<float>(0, 0), kalmanFilter.StatePost.At<float>(1, 0), kalmanFilter.StatePost.At<float>(2, 0));
			return Vector3.zero;
		}
	}

	/// <summary>
	/// Internal storage for predicted value at timestep t + dt
	/// </summary>
	private Mat kfPredictionInternal;
	
	/// <summary>
	/// Kalman Filter prediction of the position at next timestep = t + dt, using dt as time to current frame. 
	/// If the dt was fixed between frames, this would be the same as the pre-corrected estimate made at the start of the next frame.
	/// This also necessarily predicts continuous motion.
	/// Gaussian smoothing will help offset this, by mirroring the past data - one (kernel value multiplied) step forwards, multiple (lower kernel value multiplied) steps back.
	/// </summary>
	public Vector3 kfPrediction
	{
		get
		{
			if (IsRunning)
				return new Vector3(kfPredictionInternal.At<float>(0, 0), kfPredictionInternal.At<float>(1, 0), kfPredictionInternal.At<float>(2, 0));
			return Vector3.zero;
		}
	}

	/// <summary>
	/// deltaTime - initialised to something to help fill in matrices in readable way
	/// </summary>
	static float dt = 0.01f;

	// x y z dx dy dz initialised to matrix
	// I am ignoring rotation as the rotational stability of the meta is reasonable as of SDK 2.6
	float[] transitionMatrix = {1, 0, 0, dt, 0, 0,
								0, 1, 0, 0, dt, 0,
								0, 0, 1, 0, 0, dt,
								0, 0, 0, 1, 0, 0,
								0, 0, 0, 0, 1, 0,
								0, 0, 0, 0, 0, 1};

	// measurementMatrix is m x n matrix (where transition matrix is n x n in size, and the measurement we make is an m x 1 matrix
	float[] measurementMatrix = { 1, 0, 0, 0, 0, 0,
								  0, 1, 0, 0, 0, 0,
								  0, 0, 1, 0, 0, 0
								};
	// the actual measuremnt made -- m x 1
	Mat cvMeasurement;

	#region deviation notes
	// NOTE
	// error ranges observed from data	
	// measurement noise
	// the following are observed total deviation ranges
	// error in x -- generally less than 0.0021
	// error in y -- generally around 0.00414, can spike higher
	// error in z -- generally between 0.0024 and 0.004	
	#endregion

	// how inaccurate the process of setting our simulated position is
	// this should basically be error free, as i will throw the values straight into the necessary matrix
	// error in differentiation, integration for dx,dy,dz?
	public float[] processNoiseCovariance = {0, 0, 0, 0, 0, 0,
										     0, 0, 0, 0, 0, 0,
										     0, 0, 0, 0, 0, 0,
										     0, 0, 0, 1e-6f, 0, 0,
										     0, 0, 0, 0, 1e-6f, 0,
										     0, 0, 0, 0, 0, 1e-6f};

	// how inaccurate the measurments are
	// covariance recorded for measurement noise is extremely low (in the area of 1e-06)
	// but those readings are problematic, so bumped up by factor of 100
	public float[] measurementNoiseCovariance = {1e-4f, 0, 0,
												 0, 1e-4f, 0,
												 0, 0, 1e-4f};

	// guessing that initial error is low in x,y,z,
	// could actually be lower for dx,dy,dz as well because of 'hold your head still' to complete tracking.
	// setting some conservative guesses
	public float[] initialCovarianceEstimate = {1e-4f, 0, 0, 0, 0, 0,
												0, 1e-4f, 0, 0, 0, 0,
												0, 0, 1e-4f, 0, 0, 0,
												0, 0, 0, 1e-2f, 0, 0,
												0, 0, 0, 0, 1e-2f, 0,
												0, 0, 0, 0, 0, 1e-2f};
	
	/// <summary>
	/// Should the kalmanfilter run?
	/// </summary>
	[SerializeField]
	public bool shouldRun;	

	/// <summary>
	/// Was spatial mapping completed?
	/// </summary>
	bool mappingCompleted = false;

	/// <summary>
	/// Is the kalman filter actually running?
	/// </summary>
	[SerializeField]
	public bool IsRunning { get { return shouldRun && mappingCompleted; } }

	/// <summary>
	/// Should output values be logged to console?
	/// </summary>
	[SerializeField]
	public bool debugOutput;

	/// <summary>
	/// Button - log transition matrix to console
	/// </summary>
	[SerializeField]
	public bool debugTransition;

	/// <summary>
	/// Button - reset ""origin"" to the current headset position
	/// </summary>
	[SerializeField]
	public bool resetOrigin;

	/// <summary>
	/// Button - Reset the process noise covariance matrix to the (manually updated) value now in the unity inspector
	/// </summary>
	[SerializeField]
	public bool resetProcessNoiseCovariance;

	/// <summary>
	/// Button - Reset the measurement noise covariance matrix to the (manually updated) value now in the unity inspector
	/// </summary>
	[SerializeField]
	public bool resetMeasurmentNoiseCovariance;

	/// <summary>
	/// Button - Reset the initial covariance estimate matrix to the (manually updated) value now in the unity inspector
	/// </summary>
	[SerializeField]
	public bool resetInitialCovarianceEstimate;

	// Use this for initialization
	void Start ()
	{
		slamEventProvider = GameObject.FindObjectOfType<SlamLocalizer>();
		if (slamEventProvider != null)
		{
			slamEventProvider.SlamMappingCompleted.AddListener(MappingCompleted);
		}

		kalmanFilter = new KalmanFilter(6, 3);

		ReinitialiseKalmanFilter();

		if (cameraObject == null)
		{
			Debug.LogWarning("CameraObject not set to an instance of an object");
		}
		//if (gaussianSmoothing == null)
		//{
		//	Debug.LogWarning("GaussianSmoothing not set to an instance of an object. GaussianSmoothing will not be performed.");
		//}
	}

	/// <summary>
	/// Reinitialise the kalman filter with some sensible guesses
	/// </summary>
	private void ReinitialiseKalmanFilter()
	{
		// (n, m) -- for comments about variables
		kalmanFilter.Init(6, 3);

		SetTransitionMatrix(transitionMatrix);
		SetMeasurementMatrix(measurementMatrix);
		SetProcessNoiseCovarianceMatrix(processNoiseCovariance);
		SetMeasurmentNoiseCovarianceMatrix(measurementNoiseCovariance);
		SetInitialCovarianceEstimateMatrix(initialCovarianceEstimate);

		cvMeasurement = new Mat(3, 1, MatType.CV_32FC1);
		cvMeasurement.SetTo(0);

		// set initial state to x=0,y=0,z=0
		kalmanFilter.StatePost.Set(0, 0, cvMeasurement.Get<float>(0, 0));
		kalmanFilter.StatePost.Set(1, 0, cvMeasurement.Get<float>(1, 0));
		kalmanFilter.StatePost.Set(2, 0, cvMeasurement.Get<float>(2, 0));
	}
	
	/// <summary>
	/// Set the transition matrix
	/// </summary>
	/// <param name="tran">new transition matrix</param>
	private void SetTransitionMatrix(float[] tran)
	{
		Marshal.Copy(tran, 0, kalmanFilter.TransitionMatrix.Data, tran.Length);
	}

	/// <summary>
	/// Set the measurement matrix
	/// </summary>
	/// <param name="meas">new measurement matrix</param>
	private void SetMeasurementMatrix(float[] meas)
	{
		Marshal.Copy(meas, 0, kalmanFilter.MeasurementMatrix.Data, meas.Length);
	}

	/// <summary>
	/// Set the ProcessNoiseCovariance matrix
	/// </summary>
	/// <param name="cov">new matrix</param>
	private void SetProcessNoiseCovarianceMatrix(float[] cov)
	{
		Marshal.Copy(cov, 0, kalmanFilter.ProcessNoiseCov.Data, cov.Length);
	}

	/// <summary>
	/// Set the MeasurmentNoiseCovariance matrix
	/// </summary>
	/// <param name="cov">new matrix</param>
	private void SetMeasurmentNoiseCovarianceMatrix(float[] cov)
	{
		Marshal.Copy(cov, 0, kalmanFilter.MeasurementNoiseCov.Data, cov.Length);
	}

	/// <summary>
	/// Set the InitialCovarianceEstimate matrix
	/// </summary>
	/// <param name="cov">new matrix</param>
	private void SetInitialCovarianceEstimateMatrix(float[] cov)
	{
		Marshal.Copy(cov, 0, kalmanFilter.ErrorCovPost.Data, cov.Length);
	}

	/// <summary>
	/// Called when the slam mapping has completed by the ISlamEvent provider
	/// </summary>
	private void MappingCompleted()
	{
		Debug.LogWarning("MappingCompleted");
		// when mapping completed, we should immediately get the camera position to use as our origin
		if (cameraObject != null)
		{
			origin = cameraObject.transform.position;

			// set initial state accordingly
			kalmanFilter.StatePost.Set(0, 0, origin.x);
			kalmanFilter.StatePost.Set(1, 0, origin.y);
			kalmanFilter.StatePost.Set(2, 0, origin.z);

			mappingCompleted = true;
		}

		if (mappingCompleted)
			shouldRun = true;
	}

	private void Update()
	{
		RunKalmanFilter(UnityEngine.Time.deltaTime, cameraObject.transform.position);

		if (debugTransition)
		{
			AugDebugUtility.DebugString(kalmanFilter.TransitionMatrix, "Kalman filter transition matrix");

			debugTransition = false;
		}

		if (resetOrigin)
		{
			mappingCompleted = false;

			ReinitialiseKalmanFilter();

			if (cameraObject != null)
			{
				origin = cameraObject.transform.position;
			}

			MappingCompleted();

			resetOrigin = false;
		}

		if (resetProcessNoiseCovariance)
		{
			SetProcessNoiseCovarianceMatrix(processNoiseCovariance);

			resetProcessNoiseCovariance = false;
		}

		if (resetMeasurmentNoiseCovariance)
		{
			SetMeasurmentNoiseCovarianceMatrix(measurementNoiseCovariance);

			resetMeasurmentNoiseCovariance = false;
		}

		if (resetInitialCovarianceEstimate)
		{
			SetInitialCovarianceEstimateMatrix(initialCovarianceEstimate);

			resetInitialCovarianceEstimate = false;
		}
	}

	/// <summary>
	/// Run the kalman filter through one update cycle
	/// </summary>
	/// <param name="deltaTime">elapsed dt since last update</param>
	/// <param name="currentMeasurement">new measurment</param>
	void RunKalmanFilter (float deltaTime, Vector3 currentMeasurement)
	{
		//if the kalman filter is actually running
		if (IsRunning)
		{
			dt = deltaTime;

			// update the tranistion matrix with the correct dt
			kalmanFilter.TransitionMatrix.Set(0, 3, dt);
			kalmanFilter.TransitionMatrix.Set(1, 4, dt);
			kalmanFilter.TransitionMatrix.Set(2, 5, dt);

			// if we have a valid camera to get positional data from, then do kalman stuff
			if (cameraObject != null)
			{
				// make prediction
				Mat prediction = kalmanFilter.Predict();
				
				// get measurement (current camera position)
				cvMeasurement.Set(0, 0, currentMeasurement.x);
				cvMeasurement.Set(1, 0, currentMeasurement.y);
				cvMeasurement.Set(2, 0, currentMeasurement.z);

				// then feed measurement into kalmanFilter and get corrected estimate
				Mat estimated = kalmanFilter.Correct(cvMeasurement);				

				#region debugOutput
				if (debugOutput)
				{
					Vector3 offsetFromOrigin = new Vector3(origin.x - estimated.At<float>(0, 0),
														   origin.y - estimated.At<float>(1, 0),
														   origin.z - estimated.At<float>(2, 0));
					
					AugDebugUtility.DebugString(new AugDebugUtility.ConcatParams(currentMeasurement, "CurrentPos"),
												new AugDebugUtility.ConcatParams(prediction, "PredictPos"),
												new AugDebugUtility.ConcatParams(estimated, "EstimatPos"),
												new AugDebugUtility.ConcatParams(offsetFromOrigin, "OffsetOrig"));
				}
				#endregion



				// make another prediction
				// to make another prediction at the same timestep, have to multiply by the transition matrix manually
				kfPredictionInternal = kalmanFilter.TransitionMatrix * estimated;
				// if we had better data (like imu measurements) with which to make an estimate, we 'might' also be able to attempt 
				// a manual correction of this value.
				// But we don't
			}
		}
	}
}
