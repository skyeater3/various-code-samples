using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GaussianSmoothing : MonoBehaviour
{
	
	[SerializeField]
	private double fullWidthAtHalfMaximum;
	/// <summary>
	/// Higher fullWidthAtHalfMaximum (FWHM) is more stable, but has a slower response time.
	/// Response time is basically equal to FWHM seconds.
	/// While moving, it should be set low. (less than 0.5 -- 0.1 is a good start)
	/// While stationary, it should be set higher. (more than 5 works well)
	/// </summary>
	public double FullWidthAtHalfMaximum
	{
		get { return fullWidthAtHalfMaximum; }
		set
		{
			if (fullWidthAtHalfMaximum > value)
			{
				realPastValuesLL.Clear();
				realPastValuesLL.AddFirst(new TimedPosition(new Vector3(0, 0, 0), 0));
				smoothedValues.Clear();
				smoothedValues.AddFirst(new TimedPosition(new Vector3(0, 0, 0), 0));
			}
			fullWidthAtHalfMaximum = value;
		}
	}

	/// <summary>
	/// Sigma is equal to FWHM / sqrt(8 * log(2))
	/// or FWHM / (2 * sqrt(2 log(2))
	/// </summary>
	public double sigma;
	
	/// <summary>
	/// Precalculated reciprocal of 2 * sigma^2
	/// </summary>
	public double doubleSigmaSquaredReciprocal;

	/// <summary>
	/// The value at which we should stop calculating the kernel. 
	/// Used to calculate finiteKernelWidth.
	/// </summary>
	public double kernelCutoff = 0.0001;
	
	/// <summary>
	/// The width (in seconds?) at which the contribution of the kernel drops below kernelCutoff.
	/// </summary>
	public double finiteKernelWidth;

	public const float fixedDataRetentionTime = 20;

	/// <summary>
	/// Some fixed low minimum value to prevent values for FWHM and cutoff being set to, or below 0
	/// </summary>
	private const double fixedLowMinimum = 0.00001;

	/// <summary>
	/// Calculate internal data from FWHM and kernelCutoff.
	/// </summary>
	internal void CalculateInternals()
	{
		// work out sigma from FWHM
		sigma = fullWidthAtHalfMaximum / Math.Sqrt(8 * Math.Log(2));
		double doubleSigmaSquared = 2 * sigma * sigma;
		doubleSigmaSquaredReciprocal = 1 / doubleSigmaSquared;
		// calculate a finite kernel width from our desired cutoff and sigma values
		finiteKernelWidth = (Math.Sqrt(-(Math.Log(kernelCutoff) * doubleSigmaSquared)));
	}

	/// <summary>
	/// Validate FWHM and kernelCutoff values.
	/// Prevent them from being set to 0 or below.
	/// KernelCutoff also shouldbe less than 1, as that would basically discard the entirety of the kernel.
	/// </summary>
	private void OnValidate()
	{
		kernelCutoff = Math.Min(Math.Max(kernelCutoff, fixedLowMinimum), 1);
		fullWidthAtHalfMaximum = Math.Max(fullWidthAtHalfMaximum, fixedLowMinimum);
	}
	
	/// <summary>
	/// Timed Position containing position of data, deltaTime to get to this data point.
	/// </summary>
	public struct TimedPosition
	{
		public float dt;
		public Vector3 position;

		public TimedPosition(Vector3 position, float dt)
		{
			this.position = position;
			this.dt = dt;
		}
	}

	/// <summary>
	/// List of previous values that have been smoothed (as calculated at the time they were smoothed)
	/// </summary>
	private LinkedList<TimedPosition> smoothedValues = new LinkedList<TimedPosition>();
	/// <summary>
	/// total delta of all past smoothed values
	/// </summary>
	private float smoothedValuesCumulativeDelta = 0.0f;


	/// <summary>
	/// List of previous real values that have been received. Remove old values once cumulativeDelta increases beyond finiteKernelWidth.
	/// </summary>
	private LinkedList<TimedPosition> realPastValuesLL = new LinkedList<TimedPosition>();
	/// <summary>
	/// total delta of all past real values
	/// </summary>
	private float pastValuesCumulativeDelta = 0.0f;

	/// <summary>
	/// The unnormalised kernel.
	/// </summary>
	private LinkedList<double> kernel = new LinkedList<double>();

	private void Awake()
	{
		CalculateInternals();
	}

	private void Start()
	{
		CalculateInternals();
		// need to enqueue a zero value for the realPastValues and smoothedValues
		// so that when cumulativeDelta ticks past finiteKernelWidth in AddPositionAndSmooth(...),
		// there is something to remove that has had no effect on the cumulative delta
		// all other data points have an effect except the first one.
		realPastValuesLL.AddFirst(new TimedPosition(new Vector3(0, 0, 0), 0));
		smoothedValues.AddFirst(new TimedPosition(new Vector3(0, 0, 0), 0));
	}

	/// <summary>
	/// Takes incoming data and performs a gaussian smoothing operation on it.
	/// At the starting edge, before the kernel has enough data to operate on, the data is assumed to be 0.
	/// At the leading edge, the KalmanFilter predicted position is used as input for the very next frame, and all other positions up to
	/// finiteKernelwidth are filled with the mirrored, smoothed values already calculated by the gaussian smoothing operation.
	/// </summary>
	/// <param name="curPos">The current position, and elapsed dt from the last frame.</param>
	/// <param name="predPos">The predicted position, one time step into the future, and the predicted dt to get to the next frame.</param>
	public Vector3 AddPositionAndSmooth(TimedPosition curPos, TimedPosition predPos)
	{
		//pastValuesCumulativeDelta += curPos.dt;
		//smoothedValuesCumulativeDelta = predPos.dt + pastValuesCumulativeDelta; // smoothedValuesCumulativeDelta starts from prediction.dt in front of 0 time

		// Tried just keeping a running tally of dt and adjust with each step but the smoothed values delta was playing up
		// So instead have summed all stored values, each frame. This is inefficient. If anyone can figure out what's going wrong here, please fix it.

		// add currentPosition.dt to cumulative elapsed delta of pastValues queue
		// skip 1 because the first value (furthest back in time) has no effect on cumulative dt of other values
		pastValuesCumulativeDelta = curPos.dt + realPastValuesLL.Skip(1).Sum((v) => v.dt);
		// skip 1 because the first value (furthest back in time) has no effect on cumulative dt of other values
		smoothedValuesCumulativeDelta = predPos.dt + smoothedValues.Skip(1).Sum((v) => v.dt);
		
		// have left this in as proof against future errors
		//if (realPastValuesLL.Count == 0 || smoothedValues.Count == 0)
		//	Debug.Log("Error - removed too much previous data. Framerate spike?");
		
		// remove all values for which cumulative elapsed delta is greater than the desired finiteKernelWidth
		while (pastValuesCumulativeDelta > finiteKernelWidth)
		{
			realPastValuesLL.RemoveFirst();			
			pastValuesCumulativeDelta -= realPastValuesLL.First.Value.dt;

			#region debug
			//if (realPastValuesLL.Count == 0)
			//{
			//	Debug.Log("Error deleting too many values");
			//	break;
			//}
			#endregion
		}
		// clean out the smoothed values that will overrun the finiteKernelWidth as well
		while (smoothedValuesCumulativeDelta > finiteKernelWidth)
		{
			smoothedValues.RemoveFirst();
			smoothedValuesCumulativeDelta -= smoothedValues.First.Value.dt;

			#region debug
			//if (smoothedValues.Count == 0)
			//{
			//	Debug.Log("Error deleting too many values");
			//	break;
			//}
			#endregion
		}
		
		// --- Calculate kernel --- //
		// clear the kernel before proceeding
		kernel.Clear();
		// we build the kernel out from the middle <- earlier, (current, prediction), later ->
		// value over which kernel is centred is one before scaling
		kernel.AddFirst(1.0);
		// predicted value goes to the next position in the kernel
		kernel.AddLast(CalculateKernelValue(predPos.dt));

		double distanceFromKernelCenter = curPos.dt;
		// iterate over every previous value in the LL (starting with the most recent values, back to the oldest)
		int realCount = 0;
		int smoothCount = 0;
		foreach (var pastValue in realPastValuesLL.Reverse())
		{
			kernel.AddFirst(CalculateKernelValue(distanceFromKernelCenter));
			distanceFromKernelCenter += pastValue.dt;
			realCount++;

			if (distanceFromKernelCenter > finiteKernelWidth)
				break;
		}

		// iterate over all previous smoothed values until finiteKernelWidth
		// TODO: Check to see if smoothedValues.Skip(1) improves accuracy
		distanceFromKernelCenter = predPos.dt;
		foreach (var smoothedValue in smoothedValues.Reverse())
		{
			kernel.AddLast(CalculateKernelValue(distanceFromKernelCenter));
			distanceFromKernelCenter += smoothedValue.dt;
			smoothCount++;

			if (distanceFromKernelCenter > finiteKernelWidth)
				break;
		}

		// now normalise kernel
		var sum = kernel.Sum();
		var normalisedKernel = kernel.Select((k) => k / sum);

		// now that we have our normalisedKernel
		// multiply all vectors element-wise by the nk values
		Vector3 newSmoothValue = new Vector3(0, 0, 0);
		using (var nk = normalisedKernel.GetEnumerator())
		{
			// move to first value
			nk.MoveNext();

			// multiply and sum all past real values
			foreach (var pastValue in realPastValuesLL.Skip(realPastValuesLL.Count - realCount))
			{
				newSmoothValue += (pastValue.position * (float) nk.Current);
				nk.MoveNext();
			}

			// add the current real value
			newSmoothValue += curPos.position * (float) nk.Current;
			nk.MoveNext();

			// add the predicted calue
			newSmoothValue += predPos.position * (float) nk.Current;
			nk.MoveNext();

			// add all past smoothed values (as mirrored empty prediction data) 
			// -- reverse the order (so we are ascending in time), because values need to match nk data
			foreach (var smoothedValue in smoothedValues.Reverse())
			{
				newSmoothValue += smoothedValue.position * (float) nk.Current;
				nk.MoveNext();
				smoothCount--;
				if (smoothCount == 0)
					break;
			}
		}
		
		// add currentPosition to the past queue.
		realPastValuesLL.AddLast(curPos);
		smoothedValues.AddLast(new TimedPosition(newSmoothValue, curPos.dt));

		return newSmoothValue;
	}

	public void RemoveLastSmoothedPosition()
	{
		realPastValuesLL.RemoveLast();
		smoothedValues.RemoveLast();
	}

	/// <summary>
	/// Calculate the contribution of a point to the overall smoothed value.
	/// </summary>
	/// <param name="distanceFromKernelCenter">How far away in time is this value from the centre of the kernel (current t = 0)</param>
	/// <returns>The unnormalised kernel contribution (between kernelCutoff for maximum time distance and 1 for current time)</returns>
	double CalculateKernelValue(double distanceFromKernelCenter)
	{
		return Math.Exp(-(distanceFromKernelCenter * distanceFromKernelCenter * doubleSigmaSquaredReciprocal));
	}
}
